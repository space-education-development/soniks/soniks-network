# Портал СОНИКС (SONIKS Network)

Портал [СОНИКС](https://sonik.space/) – это веб-приложение, реализующее инструмент для создания открытой сети наземных станций.

Он позволяет проводить контроль и слежение за состоянием наземных станций, а также проводить с помощью них наблюдения за спутниковыми аппаратами, добавленными в базу данных сети.

[СОНИКС](https://sonik.space/) – Out-Of-Tree [SatNOGS](https://satnogs.org/) проект, который расширяет применение технологий, а также развивает их.

## Участие в разработке

Для участия в разработке можно воспользоваться инструкцией (на английском) – [SatNOGS development documentation](https://docs.satnogs.org/projects/satnogs-network/en/stable/) – о том, как настроить собственное окружение для разработки.

Главный репозиторий находится на [Gitlab](https://gitlab.com/space-education-development/soniks/soniks-network/), поэтому все Merge Requests должны располагаться здесь.

## Подключиться

[СОНИКС](https://sonik.space/):

[<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Telegram_2019_Logo.svg/240px-Telegram_2019_Logo.svg.png" width="25"/>](https://t.me/sonikspace)


[SatNOGS](https://satnogs.org/):

[![irc](https://img.shields.io/badge/Matrix-%23satnogs:matrix.org-blue.svg)](https://riot.im/app/#/room/#satnogs:matrix.org)
[![irc](https://img.shields.io/badge/IRC-%23satnogs%20on%20freenode-blue.svg)](https://webchat.freenode.net/?channels=satnogs)
[![irc](https://img.shields.io/badge/forum-discourse-blue.svg)](https://community.libre.space/c/satnogs)

## License

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
