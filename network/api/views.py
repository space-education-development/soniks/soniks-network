"""SONIKS Network API django rest framework Views"""

from datetime import datetime, timedelta, timezone
from random import choices
from string import ascii_letters, digits

from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import ContentFile
from django.db import transaction
from django.db.models import Count, F, Prefetch, Q
from django.db.models.query import QuerySet
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from rest_framework import mixins, permissions, status, viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.parsers import FileUploadParser, FormParser, MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.serializers import ValidationError

from network.api import filters, pagination, serializers
from network.api.pagination import TransmitterPageNumberPagination
from network.api.perms import SafeMethodsWithPermission, StationOwnerPermission
from network.api.renderers import BrowsableAPIRendererWithoutForms
from network.base.models import (
    DemodData,
    LatestTleSet,
    Mode,
    Observation,
    Satellite,
    Station,
    Tle,
    Transmitter,
)
from network.base.rating_tasks import rate_observation
from network.base.tasks import (
    decode_current_frame,
    delay_task_with_lock,
    process_audio,
    send_current_data_to_satnogs,
)
from network.base.utils import gridsquare
from network.base.validators import (
    NegativeElevationError,
    NoTleSetError,
    ObservationOverlapError,
    SchedulingLimitError,
    SinglePassError,
)


class ObservationView(  # pylint: disable=R0901
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.CreateModelMixin,
    viewsets.GenericViewSet,
):
    """
    SONIKS Network Observation API view class
    """

    filterset_class = filters.ObservationViewFilter
    pagination_class = pagination.ObservationCursorPagination

    def get_permissions(self):
        if self.action in ("update", "create"):
            self.permission_classes = [StationOwnerPermission]
        return super().get_permissions()

    def get_queryset(self):
        if self.action == "update":
            queryset = Observation.objects.select_for_update()
        else:
            queryset = Observation.objects.prefetch_related("demoddata").select_related(
                "satellite",
                "ground_station",
                "waterfall_status_user",
                "author",
                "transmitter",
                "tle",
            )
        if isinstance(queryset, QuerySet):
            # Ensure queryset is re-evaluated on each request.
            queryset = queryset.all()
        return queryset

    def get_serializer_class(self):
        """
        Returns the right serializer depending on http method that is used
        """
        if self.action == "create":
            return serializers.NewObservationSerializer
        if self.action == "update":
            return serializers.UpdateObservationSerializer
        return serializers.ObservationSerializer

    def create(self, request, *args, **kwargs):
        """
        Creates observations from a list of observation data
        """
        serializer = self.get_serializer(
            data=request.data, many=True, allow_empty=False
        )
        try:
            if serializer.is_valid():
                observations = serializer.save()
                serialized_obs = serializers.ObservationSerializer(
                    observations, many=True
                )
                data = serialized_obs.data
                response = Response(data, status=status.HTTP_200_OK)
            else:
                data = serializer.errors
                response = Response(data, status=status.HTTP_400_BAD_REQUEST)
        except (
            NegativeElevationError,
            SinglePassError,
            ValidationError,
            ValueError,
        ) as error:
            response = Response(str(error), status=status.HTTP_400_BAD_REQUEST)
        except NoTleSetError as error:
            response = Response(str(error), status=status.HTTP_501_NOT_IMPLEMENTED)
        except (ObservationOverlapError, SchedulingLimitError) as error:
            response = Response(str(error), status=status.HTTP_409_CONFLICT)
        return response

    def update(self, request, *args, **kwargs):
        """
        Updates observation with audio, waterfall or demoded data
        """
        observation_has_data = False
        demoddata_id = None
        with transaction.atomic():
            instance = self.get_object()
            if request.data.get("client_version"):
                instance.ground_station.client_version = request.data.get(
                    "client_version"
                )
                instance.ground_station.save()
            if request.data.get("demoddata"):
                name = "data_obs/{0}/{1}/{2}/{3}/{4}/{5}".format(
                    instance.start.year,
                    instance.start.month,
                    instance.start.day,
                    instance.start.hour,
                    instance.id,
                    request.data.get("demoddata"),
                )
                try:
                    instance.demoddata.get(demodulated_data=name)
                    return Response(
                        data="This data file has already been uploaded",
                        status=status.HTTP_403_FORBIDDEN,
                    )
                except ObjectDoesNotExist:
                    # Check if observation has data before saving the current ones
                    observation_has_data = instance.demoddata.exists()
                    sat = instance.satellite
                    gr_station = instance.ground_station
                    file_datetime = name.split("/")[-1].split("_")[2].split(".")[0]
                    try:
                        frame_datetime = datetime.strptime(
                            file_datetime, "%Y-%m-%dT%H-%M-%S"
                        )
                    except ValueError:
                        frame_datetime = datetime.strptime(
                            file_datetime, "%Y-%m-%dT%H:%M:%S"
                        )
                    submit_datetime = datetime.strftime(
                        frame_datetime, "%Y-%m-%dT%H:%M:%S.000Z"
                    )
                    demoddata_serializer = serializers.CreateDemodDataSerializer(
                        data={
                            "observation": instance.pk,
                            "demodulated_data": request.data.get("demoddata"),
                            "satellite": sat.pk,
                            "station": gr_station.pk,
                            "timestamp": submit_datetime,
                            "observer": instance.author.displayname,
                            "lat": gr_station.lat,
                            "lng": gr_station.lng,
                        }
                    )
                    demoddata_serializer.is_valid(raise_exception=True)
                    demoddata = demoddata_serializer.save()
                    demoddata_id = demoddata.id
            if request.data.get("waterfall"):
                if instance.has_waterfall:
                    return Response(
                        data="Watefall has already been uploaded",
                        status=status.HTTP_403_FORBIDDEN,
                    )
            if request.data.get("payload"):
                if instance.has_audio:
                    return Response(
                        data="Audio has already been uploaded",
                        status=status.HTTP_403_FORBIDDEN,
                    )

            # False-positive no-member (E1101) pylint error:
            # Parent class rest_framework.mixins.UpdateModelMixin provides the 'update' method
            super().update(request, *args, **kwargs)  # pylint: disable=E1101

        if request.data.get("waterfall"):
            rate_observation.delay(instance.id, "waterfall_upload")
        # Rate observation only on first demoddata uploading
        if request.data.get("demoddata") and demoddata_id:
            decode_current_frame.delay(sat_id=sat.sat_id, demoddata_id=demoddata_id)
            send_current_data_to_satnogs.delay(data_id=demoddata_id)
            if not observation_has_data:
                rate_observation.delay(instance.id, "data_upload")
        if request.data.get("payload"):
            delay_task_with_lock(
                process_audio,
                instance.id,
                settings.PROCESS_AUDIO_LOCK_EXPIRATION,
                instance.id,
            )
        return Response(status=status.HTTP_200_OK)


class StationView(  # pylint: disable=R0901
    mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    """
    SONIKS Network Station API view class
    """

    queryset = (
        Station.objects.annotate(
            total_obs=Count("observations"),
        )
        .order_by("id")
        .prefetch_related(
            "antennas", "antennas__antenna_type", "antennas__frequency_ranges"
        )
    )
    serializer_class = serializers.StationSerializer
    filterset_class = filters.StationViewFilter
    pagination_class = pagination.StationViewPageNumberPagination


class SatelliteView(  # pylint: disable=R0901
    mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    """
    SONIKS Network Satellite API view class
    """

    queryset = Satellite.objects.prefetch_related(
        "operator",
        "network",
        "created_by",
        "launch",
    ).order_by("norad_cat_id")
    serializer_class = serializers.SatelliteSerializer
    filterset_class = filters.SatelliteViewFilter


@api_view(["POST"])
@permission_classes((AllowAny,))
def station_register_view(request):
    """
    API endpoint for receiving client_id and return url for registering new station or connect \\
    client_id to existing one
    """
    client_id = request.POST.get("client_id", None)
    if client_id:
        if Station.objects.filter(client_id=client_id).exists():
            error = (
                'Invalid Client ID, please restart the "Station Registration" process.'
            )
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        url_hash = "".join(choices(ascii_letters + digits, k=60))
        cache.set(url_hash, client_id, 60 * 10)
        path = (
            reverse("base:station_register", kwargs={"step": 1}) + "?hash=" + url_hash
        )
        url = request.build_absolute_uri(path)
        return Response(data={"url": url}, status=status.HTTP_200_OK)
    return HttpResponseRedirect(redirect_to="/api/")


class TransmitterView(  # pylint: disable=R0901
    mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    """
    SONIKS Network Transmitter API view class
    """

    queryset = (
        Transmitter.objects.prefetch_related(
            "satellite", "downlink_mode", "uplink_mode", "stat"
        )
        .annotate(
            future_count=Count("observations", filter=Q(observations__end__gt=now()))
        )
        .order_by("status")
    )
    serializer_class = serializers.TransmitterSerializer
    filterset_class = filters.TransmitterViewFilter

    @property
    def pagination_class(self):
        """Returns pagination class if request from API"""
        if self.request.headers.get("x-requested-with") != "XMLHttpRequest":
            return TransmitterPageNumberPagination
        return None


class JobView(viewsets.ReadOnlyModelViewSet):  # pylint: disable=R0901
    """
    SONIKS Network Job API view class
    """

    queryset = Observation.objects.all().prefetch_related(
        "transmitter", "transmitter__downlink_mode", "tle"
    )
    filterset_class = filters.ObservationViewFilter
    serializer_class = serializers.JobSerializer
    filterset_fields = "ground_station"

    def list(self, request, *args, **kwargs):
        lat = self.request.query_params.get("lat", None)
        lon = self.request.query_params.get("lon", None)
        alt = self.request.query_params.get("alt", None)
        ground_station_id = self.request.query_params.get("ground_station", None)
        if ground_station_id and self.request.user.is_authenticated:
            ground_station = get_object_or_404(Station, id=ground_station_id)
            if ground_station.owner == self.request.user:
                if not (lat is None or lon is None or alt is None):
                    data = {"lat": lat, "lng": lon, "altitude": alt, "last_seen": now()}
                else:
                    data = {"last_seen": now()}

                serializer = serializers.StationSerializer(
                    ground_station, data=data, partial=True
                )
                if serializer.is_valid() is False:
                    return Response(
                        serializer.errors, status=status.HTTP_400_BAD_REQUEST
                    )

                serializer.save()

        job_serializer = serializers.JobSerializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(job_serializer.data, status=status.HTTP_200_OK)

    def get_queryset(self):
        """
        Returns queryset for Job API view
        """
        queryset = self.queryset.filter(start__gte=now())

        return queryset


class DemodDataViewSet(  # pylint: disable=R0901,R0912,R0915
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    viewsets.GenericViewSet,
):
    """
    SONIKS Network Demoddata API view class
    """

    queryset = DemodData.objects.all().prefetch_related(
        "satellite", "transmitter", "observation"
    )
    serializer_class = serializers.DemodDataViewSerializer
    filterset_class = filters.DemodDataViewFilter
    permission_classes = [SafeMethodsWithPermission]
    parser_classes = (FormParser, MultiPartParser, FileUploadParser)
    pagination_class = pagination.DemodDataCursorPagination
    renderer_classes = (BrowsableAPIRendererWithoutForms, JSONRenderer)

    def list(self, request, *args, **kwargs):
        """
        List method for DemodDataViewSet
        Returns a list of demod data based on filters provided in the query parameters.
        If no valid filter is provided, returns a 400 error response with a detail message.
        """
        satellite = request.query_params.get("satellite", None)
        sat_id = request.query_params.get("sat_id", None)
        observation_id = request.query_params.get("observation_id", None)
        station_id = request.query_params.get("station_id", None)
        if not (satellite or sat_id or observation_id or station_id):
            data = {
                "detail": (
                    "For getting data please use either satellite(NORAD ID) filter or "
                    "sat_id(Satellite ID) filter or Observation ID filter or statiod ID filter"
                ),
                "results": None,
            }
            response = Response(data, status=status.HTTP_400_BAD_REQUEST)
            response.exception = True
            return response

        return super().list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        data = {}
        norad_id = request.data.get("noradID")

        try:
            if norad_id:
                satellite = Satellite.objects.get(norad_cat_id=norad_id)
            else:
                raise ValueError
        except (Satellite.DoesNotExist, ValueError):
            return Response(
                "Not found NORAD or satellite doesn't exist",
                status=status.HTTP_400_BAD_REQUEST,
            )

        data["satellite"] = satellite.id
        observer = request.data.get("source")
        data["timestamp"] = request.data.get("timestamp")
        if request.data.get("version"):
            data["version"] = request.data.get("version")
        if request.data.get("observation_id"):
            observation_id = request.data.get("observation_id")
            try:
                observation = Observation.objects.get(id=observation_id)
                data["observation"] = observation.id
            except Observation.DoesNotExist:
                pass
        if request.data.get("station_id"):
            station_id = request.data.get("station_id")
            try:
                station = Station.objects.get(id=station_id)
                data["station"] = station.id
            except Station.DoesNotExist:
                pass

        try:
            lat = request.data["latitude"]
            lng = request.data["longitude"]
        except KeyError as err:
            return Response(
                f"Missing request parameter: '{err.args[0]}'",
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            if any(x.isalpha() for x in lat):
                data["lat"] = -float(lat[:-1]) if ("S" in lat) else float(lat[:-1])
            else:
                data["lat"] = float(lat)
            if any(x.isalpha() for x in lng):
                data["lng"] = -float(lng[:-1]) if ("W" in lng) else float(lng[:-1])
            else:
                data["lng"] = float(lng)
        except ValueError:
            return Response(
                "Ошибка в переданных координатах", status=status.HTTP_400_BAD_REQUEST
            )

        frame = ContentFile(request.data.get("frame"), name="sids")
        data["demodulated_data"] = frame

        qth = gridsquare(data["lat"], data["lng"])
        observer = "{0}-{1}".format(observer, qth)
        data["observer"] = observer
        data["sonik_data"] = False

        serializer = serializers.SidsSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        decode_current_frame.delay(satellite.sat_id, serializer.instance.pk)

        return Response("OK!", status=status.HTTP_201_CREATED)


class ModeView(  # pylint: disable=R0901
    viewsets.ReadOnlyModelViewSet
):
    """
    SONIKS Network Mode API view class
    """

    queryset = Mode.objects.all()
    serializer_class = serializers.ModeSerializer


class TleView(  # pylint: disable=R0901
    viewsets.ReadOnlyModelViewSet
):
    """
    SONIKS Network Tle API view class
    """

    queryset = Tle.objects.all().prefetch_related("satellite").order_by("-updated")
    serializer_class = serializers.TleViewSerializer
    filterset_class = filters.TleViewFilter
    pagination_class = pagination.TleCursorPagination


class LatestTleView(  # pylint: disable=R0901
    viewsets.ReadOnlyModelViewSet
):
    """
    SONIKS Network LatestTle API view class
    """

    queryset = (
        LatestTleSet.objects.all()
        .select_related("satellite", "latest")
        .only(
            "last_modified",
            "satellite__name",
            "satellite__sat_id",
            "satellite__norad_cat_id",
            "latest",
        )
        .order_by("satellite__norad_cat_id")
    )
    serializer_class = serializers.LatestTleViewSerializer
    filterset_class = filters.LatestTleViewFilter


@api_view(["GET"])
@permission_classes((AllowAny,))
def satellite_obs_on_stations(request):
    satellite_norad = request.GET.get("satellite_norad", None)
    if satellite_norad is None:
        return Response(_("Предоставьте норад спутника, пожалуйста"))
    stations = (
        Station.objects.prefetch_related(
            Prefetch(
                "observations",
                queryset=Observation.objects.filter(
                    start__gte=datetime.now(timezone.utc),
                    satellite__norad_cat_id=satellite_norad,
                ),
            )
        )
        .filter(
            observations__isnull=False,
            observations__satellite__norad_cat_id=satellite_norad,
        )
        .only("pk", "name", "lat", "lng")
        .distinct()
        .order_by("pk")
    )
    output_json = {}
    for station in stations:
        station_params = {}
        station_params["lat"] = station.lat
        station_params["lon"] = station.lng
        obs_all_time = []
        for obs in station.observations.all():
            single_obs_time = []
            single_obs_time.append(
                datetime(
                    year=obs.start.year,
                    month=obs.start.month,
                    day=obs.start.day,
                    hour=obs.start.hour,
                    minute=obs.start.minute,
                    second=obs.start.second,
                ),
            )
            single_obs_time.append(
                datetime(
                    year=obs.end.year,
                    month=obs.end.month,
                    day=obs.end.day,
                    hour=obs.end.hour,
                    minute=obs.end.minute,
                    second=obs.end.second,
                ),
            )
            obs_all_time.append(single_obs_time)
        station_params["obs"] = obs_all_time
        output_json[station.id] = station_params
    return JsonResponse(output_json)


from rest_framework.decorators import api_view


@api_view(["GET"])
@permission_classes((permissions.AllowAny,))
def top_data_view(request):
    reference_time_date = now()

    day_ago = reference_time_date - timedelta(days=1)
    week_ago = reference_time_date - timedelta(days=7)
    month_ago = reference_time_date - timedelta(days=30)

    # satellite info
    top_sat_sonik_d = (
        Satellite.objects.annotate(
            count=Count(
                "telemetry_data",
                filter=(
                    Q(telemetry_data__sonik_data=True)
                    & Q(telemetry_data__timestamp__gt=day_ago)
                ),
            )
        )
        .order_by("-count")[:10]
        .values("name", "count", "sat_id")
    )
    top_sat_sonik_w = (
        Satellite.objects.annotate(
            count=Count(
                "telemetry_data",
                filter=(
                    Q(telemetry_data__sonik_data=True)
                    & Q(telemetry_data__timestamp__gt=week_ago)
                ),
            )
        )
        .order_by("-count")[:10]
        .values("name", "count", "sat_id")
    )
    top_sat_sonik_m = (
        Satellite.objects.annotate(
            count=Count(
                "telemetry_data",
                filter=(
                    Q(telemetry_data__sonik_data=True)
                    & Q(telemetry_data__timestamp__gt=month_ago)
                ),
            )
        )
        .order_by("-count")[:10]
        .values("name", "count", "sat_id")
    )

    # station info
    top_station_sonik_d = (
        DemodData.objects.filter(
            Q(station__isnull=False) & Q(sonik_data=True) & Q(timestamp__gt=day_ago)
        )
        .values("station__name", "station__id")
        .annotate(
            count=Count("station__name"),
            name=F("station__name"),
            id=F("station__id"),
        )
        .order_by("-count")[:10]
        .values("name", "id", "count")
    )
    top_station_sonik_w = (
        DemodData.objects.filter(
            Q(station__isnull=False) & Q(sonik_data=True) & Q(timestamp__gt=week_ago)
        )
        .values("station__name", "station__id")
        .annotate(
            count=Count("station__name"),
            name=F("station__name"),
            id=F("station__id"),
        )
        .order_by("-count")[:10]
        .values("name", "id", "count")
    )
    top_station_sonik_m = (
        DemodData.objects.filter(
            Q(station__isnull=False) & Q(sonik_data=True) & Q(timestamp__gt=month_ago)
        )
        .values("station__name", "station__id")
        .annotate(
            count=Count("station__name"),
            name=F("station__name"),
            id=F("station__id"),
        )
        .order_by("-count")[:10]
        .values("name", "id", "count")
    )

    output_json = {
        "top_sat_sonik_d": list(top_sat_sonik_d),
        "top_sat_sonik_w": list(top_sat_sonik_w),
        "top_sat_sonik_m": list(top_sat_sonik_m),
        "top_station_sonik_d": list(top_station_sonik_d),
        "top_station_sonik_w": list(top_station_sonik_w),
        "top_station_sonik_m": list(top_station_sonik_m),
    }
    return JsonResponse(output_json)
