"""SONIKS Network django rest framework Filters class"""

import django
import django_filters
from django.shortcuts import get_object_or_404
from django.utils.timezone import now
from django_filters.rest_framework import FilterSet

from network.base.models import (
    DemodData,
    LatestTleSet,
    Observation,
    Satellite,
    Station,
    Tle,
    Transmitter,
)
from network.base.validators import is_transmitter_in_station_range
from network.users.models import User


class NumberInFilter(django_filters.BaseInFilter, django_filters.NumberFilter):
    """
    Filter for comma separated numbers
    """


class ObservationViewFilter(FilterSet):
    """
    SONIKS Network Observation API View Filter
    """

    OBSERVATION_STATUS_CHOICES = [
        ("failed", "Failed"),
        ("bad", "Bad"),
        ("unknown", "Unknown"),
        ("future", "Future"),
        ("good", "Good"),
    ]

    WATERFALL_STATUS_CHOICES = [
        (1, "With Signal"),
        (0, "Without Signal"),
    ]

    # DEPRECATED
    VETTED_STATUS_CHOICES = [
        ("failed", "Failed"),
        ("bad", "Bad"),
        ("unknown", "Unknown"),
        ("good", "Good"),
    ]

    start = django_filters.IsoDateTimeFilter(field_name="start", lookup_expr="gte")
    end = django_filters.IsoDateTimeFilter(field_name="end", lookup_expr="lte")
    status = django_filters.ChoiceFilter(
        field_name="status", choices=OBSERVATION_STATUS_CHOICES, method="filter_status"
    )
    waterfall_status = django_filters.ChoiceFilter(
        field_name="waterfall_status",
        choices=WATERFALL_STATUS_CHOICES,
        null_label="Unknown",
    )
    vetted_status = django_filters.ChoiceFilter(
        label="Vetted status (deprecated: please use Status)",
        field_name="status",
        choices=VETTED_STATUS_CHOICES,
        method="filter_status",
    )
    vetted_user = django_filters.ModelChoiceFilter(
        label="Vetted user (deprecated: will be removed in next version)",
        field_name="waterfall_status_user",
        queryset=User.objects.all(),
    )

    observer = django_filters.ModelChoiceFilter(
        label="observer",
        field_name="author",
        queryset=User.objects.filter(observations__isnull=False).distinct(),
    )

    observation_id = NumberInFilter(field_name="id", label="Observation ID(s)")

    # see https://django-filter.readthedocs.io/en/master/ref/filters.html for W0613
    def filter_status(self, queryset, name, value):  # pylint: disable=W0613,R0201
        """Returns filtered observations for a given observation status"""
        if value == "failed":
            observations = queryset.filter(status__lt=-100)
        if value == "bad":
            observations = queryset.filter(status__range=(-100, -1))
        if value == "unknown":
            observations = queryset.filter(status__range=(0, 99), end__lte=now())
        if value == "future":
            observations = queryset.filter(end__gt=now())
        if value == "good":
            observations = queryset.filter(status__gte=100)
        return observations

    class Meta:
        model = Observation
        fields = [
            "id",
            "status",
            "ground_station",
            "start",
            "end",
            "satellite__norad_cat_id",
            "transmitter__uuid",
            "transmitter__downlink_mode",
            "transmitter__type",
            "waterfall_status",
            "vetted_status",
            "vetted_user",
            "observer",
        ]


class StationViewFilter(FilterSet):
    """
    SONIKS Network Station API View Filter
    """

    class Meta:
        model = Station
        fields = ["id", "name", "status", "client_version"]


class SatelliteViewFilter(FilterSet):
    """
    SONIKS Network Satellite API View Filter
    """

    station_id = django_filters.NumberFilter(
        field_name="station_id", method="in_station_range", label="In range of station"
    )

    # pylint: disable=W0613,R0201
    def in_station_range(self, queryset, field_name, station_id: int):
        """
        Check if satellite transmitters freq in range of station antennas

        Args:
            station_id (int): ID if station which antennas range checking

        """
        if station_id:
            satellites_in_range: list[int] = []
            station = get_object_or_404(
                Station.objects.prefetch_related(
                    "antennas", "antennas__frequency_ranges"
                ),
                pk=station_id,
            )
            transmitters = (
                Transmitter.objects.all()
                .exclude(status="invalid", downlink_low=None)
                .prefetch_related("satellite")
                .only("id", "downlink_high", "downlink_low", "satellite__id", "type")
            )
            for transmitter in transmitters:
                if is_transmitter_in_station_range(transmitter, station):
                    satellites_in_range.append(transmitter.satellite.id)
        return queryset.filter(pk__in=satellites_in_range)

    class Meta:
        model = Satellite
        fields = [
            "sat_id",
            "call_sign",
            "norad_cat_id",
            "name",
            "status",
            "countries",
            "network",
            "operator",
            "unknown",
            "is_frequency_violator",
            "launch__id",
        ]


class TransmitterViewFilter(FilterSet):
    """
    SONIKS Network Transmitter API View Filter
    """

    station_id = django_filters.NumberFilter(
        field_name="station_id", method="in_station_range", label="In range of station"
    )

    # pylint: disable=W0613,R0201
    def in_station_range(self, queryset, field_name, station_id: int):
        """
        Check if transmitter freq in range of station antennas

        Args:
            station_id (int): ID if station which antennas range checking

        """
        if station_id:
            station = get_object_or_404(
                Station.objects.prefetch_related(
                    "antennas", "antennas__frequency_ranges"
                ),
                pk=station_id,
            )
            transmitters_include: set[int] = set()
            for transmitter in queryset:
                if is_transmitter_in_station_range(transmitter, station):
                    transmitters_include.add(transmitter.uuid)
        return queryset.filter(uuid__in=transmitters_include)

    class Meta:
        model = Transmitter
        fields = [
            "uuid",
            "description",
            "satellite__name",
            "satellite__norad_cat_id",
            "satellite__sat_id",
            "satellite__launch__id",
            "type",
            "status",
            "approved",
            "unknown",
        ]


class DemodDataViewFilter(FilterSet):
    """
    SONIKS Network Demoddata API View Filter
    """

    id = django_filters.NumberFilter(field_name="id")
    satellite = django_filters.NumberFilter(
        field_name="satellite__norad_cat_id",
        method="get_demoddata_from_norad_id",
        label="Satellite NORAD ID",
    )
    sat_id = django_filters.CharFilter(
        field_name="satellite__sat_id",
        method="get_demoddata_from_sat_id",
        label="Satellite ID",
    )
    observation_id = django_filters.NumberFilter(
        field_name="observation__id",
        method="get_demoddata_from_observation",
        label="Observation ID",
    )
    station_id = django_filters.NumberFilter(
        field_name="station__id",
        method="get_demoddata_from_station",
        label="Station ID",
    )
    start = django_filters.IsoDateTimeFilter(field_name="timestamp", lookup_expr="gte")
    end = django_filters.IsoDateTimeFilter(field_name="timestamp", lookup_expr="lte")

    # pylint: disable=W0613,R0201
    def queryset_from_satellite(self, queryset, satellite: Satellite):
        """
        Returns a filtered queryset by the satellite and its associated ones

        Args:
            satellite(Satellite): Instance of satellite which data we need

        Returns:
            Queryset: Queryset filtered by satellite
        """
        return queryset.filter(satellite=satellite)

    # pylint: disable=W0613,R0201
    def get_demoddata_from_norad_id(self, queryset, field_name, norad_id: int):
        """
        Return DemodData that belong to the satellite or its associated ones

        Args:
            norad_id(int): Norad id of satellite which data we need
        """
        if norad_id:
            satellite = get_object_or_404(Satellite, norad_cat_id=norad_id)
            return self.queryset_from_satellite(queryset, satellite)
        return queryset

    # pylint: disable=W0613,R0201
    def get_demoddata_from_sat_id(self, queryset, field_name, sat_id: str):
        """
        Return DemodData that belong to the satellite or its associated ones

        Args:
            sat_id(str): Sat id of satellite which data we need
        """
        if sat_id:
            satellite = get_object_or_404(Satellite, sat_id=sat_id)
            return self.queryset_from_satellite(queryset, satellite)
        return queryset

    def get_demoddata_from_observation(self, queryset, field_name, observation_id: int):
        """
        Return DemodData than belong to the target observation

        Args:
            observation_id (int): ID of observation which data we need
        """
        if observation_id:
            observation = get_object_or_404(Observation, pk=observation_id)
            return queryset.filter(observation=observation)
        return queryset

    def get_demoddata_from_station(self, queryset, field_name, station_id: int):
        """
        Return DemodData that belong to the target station

        Args:
            station_id (int): ID of station which data we need
        """
        if station_id:
            station = get_object_or_404(Station, pk=station_id)
            return queryset.filter(station=station)
        return queryset

    class Meta:
        model = DemodData
        fields = ["observer", "transmitter", "is_decoded"]


class TleViewFilter(FilterSet):
    """
    SONIKS Network Tle API View Filter
    """

    satellite_name = django_filters.CharFilter(
        field_name="satellite__name",
        method="get_tle_from_satellite_name",
        label="Satellite name",
    )
    satellite_norad = django_filters.NumberFilter(
        field_name="satellite__norad_cat_id",
        method="get_tle_from_satellite_norad",
        label="Satellite NORAD",
    )

    satellite_sat_id = django_filters.CharFilter(
        field_name="satellite__sat_id",
        method="get_tle_from_satellite_sat_id",
        label="Satellite sat id",
    )
    updated_from = django_filters.IsoDateTimeFilter(
        field_name="updated", lookup_expr="gte"
    )
    updated_to = django_filters.IsoDateTimeFilter(
        field_name="updated", lookup_expr="lte"
    )

    # pylint: disable=W0613,R0201
    def get_tle_from_satellite_name(self, queryset, field_name, satellite_name: str):
        """
        Get all TLE of satellite by his name

        Args:
            satellite_name (str): Name of satellite which TLE we need

        """
        return queryset.filter(satellite__name=satellite_name)

    # pylint: disable=W0613,R0201
    def get_tle_from_satellite_norad(self, queryset, field_name, satellite_norad: int):
        """
        Get all TLE of satellite by his norad

        Args:

            satellite_norad (int): NORAD of satellite which TLE we need

        """
        return queryset.filter(satellite__norad_cat_id=satellite_norad)

    # pylint: disable=W0613,R0201
    def get_tle_from_satellite_sat_id(
        self, queryset, field_name, satellite_sat_id: str
    ):
        """
        Get all TLE of satellite by his sat_id

        Args:
            satellite_sat_id (str): Sat id of satellite which TLE we need

        """
        return queryset.filter(satellite__sat_id=satellite_sat_id)

    class Meta:
        model = Tle
        fields = ["tle_source", "satellite"]


class LatestTleViewFilter(FilterSet):
    """
    SONIKS Network LatestTle API View Filter
    """

    start = django_filters.IsoDateTimeFilter(
        field_name="last_modified", lookup_expr="gte"
    )
    end = django_filters.IsoDateTimeFilter(
        field_name="last_modified", lookup_expr="lte"
    )

    class Meta:
        model = LatestTleSet
        fields = ["satellite__name", "satellite__sat_id", "satellite__norad_cat_id"]
