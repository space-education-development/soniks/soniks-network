"""SONIKS Network InfluxDB"""

import binascii
import json
import re
from datetime import datetime, timedelta, timezone
from typing import Optional

from django.conf import settings
from django.core.files.base import ContentFile
from django.db import transaction
from django.utils.timezone import make_aware
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS
from satnogsdecoders import __version__ as decoders_version
from satnogsdecoders import decoder

from network.base.models import DemodData, Satellite, Telemetry


def decode_data(
    sat_id: str,
    demoddata_id: Optional[int] = None,
    redecode: bool = False,
    decode_last_week: bool = False,
):
    """
    Decode a DemodData object, store decoded data in payload json and push it in InfluxDB if INFLUX is setted.
    It always pushed in local DB

    Args:
        sat_id(str): Sat id of satellite which data is decoding
        demoddata_id(int): ID of demoddata which decoding
        redecode(bool): If True, redecode all data
        decode_last_week(bool): If True, decode only data from last week
    """
    satellite = Satellite.objects.get(sat_id=sat_id)
    telemetry_decoders = satellite.telemetries.all()
    if not telemetry_decoders:
        return

    demoddata_queryset = DemodData.objects.filter(
        satellite=satellite, is_image=False
    ).order_by("-id")
    if demoddata_id:  # choose one demoddata instead full list
        demoddata_queryset = demoddata_queryset.filter(pk=demoddata_id)
    if decode_last_week:  # decode only data from last week
        time_period = make_aware(datetime.now(timezone.utc) - timedelta(hours=168))
        demoddata_queryset = demoddata_queryset.filter(timestamp__gte=time_period)
    if not redecode:  # decode only undecoded data
        demoddata_queryset = demoddata_queryset.filter(is_decoded=False)
    print("demoddata len is {}".format(demoddata_queryset.count()))
    for demoddata in demoddata_queryset:
        for telemetry_decoder in telemetry_decoders:
            print("decode data with id {}".format(demoddata.id))
            try:
                decode_demoddata(
                    demoddata=demoddata,
                    satellite=satellite,
                    telemetry=telemetry_decoder,
                )
            except Exception:
                continue  # skip broken demoddata


def decode_demoddata(demoddata: DemodData, satellite: Satellite, telemetry: Telemetry):
    """
    Decode a Demoddata object,store decoded data in payload json and push it in InfluxDB \\
    if INFLUX is setted.It always pushed in local DB

    Args:
        demoddata(DemodData): Instance of demoddata which should be decoded
        satellite(Satellite): Instance of satellite which data is decoding
        telemetry(Telemetry): Instance of telemetry which used for decoding
    """
    try:
        decoder_class = getattr(decoder, telemetry.decoder.capitalize())
    except AttributeError:
        return

    try:
        with demoddata.demodulated_data.storage.open(
            demoddata.demodulated_data.name, "r"
        ) as file:
            data = file.read()
            if re.fullmatch(r"^[A-F0-9]+$", data):
                bindata_test = binascii.unhexlify(data)
            else:
                raise TypeError
    except (TypeError, UnicodeDecodeError):
        hexdata_test = demoddata.display_payload_hex().replace(" ", "")
        bindata_test = binascii.unhexlify(hexdata_test)
    with transaction.atomic():
        try:
            frame = decoder_class.from_bytes(bindata_test)
            fields = decoder.get_fields(frame)
            if len(fields) > 6:
                influx_point = create_point(
                    fields,
                    satellite,
                    telemetry,
                    demoddata,
                    decoders_version,
                )
                if settings.USE_INFLUX:
                    write_influx(influx_point)
                data = DemodData.objects.select_for_update().get(pk=demoddata.id)
                json_point = json.dumps(influx_point)
                json_file = ContentFile(json_point.encode("utf-8"))
                data.is_decoded = True
                data_name = data.demodulated_data.name.split("/")[-1]
                data.payload_json.save(f"{data_name}.json", json_file)
                print("data with id {} has been decoded".format(demoddata.id))
        except BaseException:  # pylint: disable=W0703
            print("error while decode data with id {}".format(demoddata.id))
            return


def create_point(
    fields: dict,
    satellite: Satellite,
    telemetry: Telemetry,
    demoddata: DemodData,
    version: str,
):
    """
    Create a decoded data  in influxdb point compatible format

    Args:
        fields(dict): Decoded fields from demoddata
        satellite(Satellite): Instance of satellite which point is created
        telemetry(Telemetry): Instance of telemetry which used for decoding
        demoddata(Demoddata): Instance of demoddata whic was decoded
        version(str): Version from satnogsdecoders

    Returns:
        list[dict]: InfluxDB point

    """

    point = [
        {
            "time": demoddata.timestamp.strftime("%Y-%m-%dT%H:%M:%SZ"),
            "measurement": satellite.norad_cat_id,
            "tags": {
                "satellite": satellite.name,
                "decoder": telemetry.decoder,
                "version": version,
            },
            "fields": fields,
        }
    ]
    if demoddata.station is not None:
        point[0]["tags"]["station"] = demoddata.station.name
    elif demoddata.observer != "":
        point[0]["tags"]["station"] = demoddata.observer.split("-")[0]
    else:
        point[0]["tags"]["station"] = "SatNOGS"
    if demoddata.observer != "":
        point[0]["tags"]["observer"] = demoddata.observer
    return point


def write_influx(point: list):
    """
    Take a Point object and send to influxdb

    Args:
        point(list): list with dict in influxpoint format
    """
    client = InfluxDBClient(
        url=settings.INFLUX_URL, token=settings.INFLUX_TOKEN, org=settings.INFLUX_ORG
    )
    write_api = client.write_api(write_options=SYNCHRONOUS)
    write_api.write(
        bucket=settings.INFLUX_BUCKED, org=settings.INFLUX_ORG, record=point
    )
