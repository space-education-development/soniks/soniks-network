"""SONIKS Network django context processors"""

from django.conf import settings
from django.template.loader import render_to_string

from network import __version__
from network.base.stats import unknown_observations_count


def stage_notice(request):
    """
    If we are in test environment, shows a yellow notice with white text
    at the top of every page, warning user that they are in test environment.
    """
    if settings.ENVIRONMENT == "test":
        return {"stage_notice": render_to_string("includes/stage_notice.html")}
    return {"stage_notice": ""}


def user_processor(request):
    """
    Returns number of user's observations with unknown status
    """
    if request.user.is_authenticated:
        owner_unknown_count = unknown_observations_count(request.user)
        return {"owner_unknown_count": owner_unknown_count}
    return {"owner_unknown_count": ""}


def auth_block(request):
    """
    Displays auth links local vs openid
    """
    if settings.OIDC_AUTH:
        return {"auth_block": render_to_string("includes/auth_openid.html")}
    return {"auth_block": render_to_string("includes/auth_local.html")}


def account_block(request):
    """
    Displays account links local vs openid
    """
    if settings.OIDC_AUTH:
        return {"account_block": render_to_string("includes/account_openid.html")}
    return {"account_block": render_to_string("includes/account_local.html")}


def version(request):
    """
    Displays the current soniks-network version
    """
    return {"version": "Version: {}".format(__version__)}
