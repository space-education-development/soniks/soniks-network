"""Django base views for SONIKS Network"""

from datetime import datetime, timedelta
from typing import List

from django.db.models import Prefetch
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.timezone import now

from network.base.decorators import ajax_required
from network.base.models import DemodData, Observation, Satellite


def db_view(request):
    """
    View to render DB page
    """
    satellites = (
        Satellite.objects.filter(
            status="alive",
            altitude__gt=120,
            decay_date__gte=now(),
            latest_tle_set__latest__updated__gt=(now() - timedelta(days=14)),
        )
        .prefetch_related("created_by", "operator", "network")
        .order_by("decay_date")[:50]
    )

    if now().hour > 5:
        table_date = datetime(now().year, now().month, now().day, 5, 0)
    else:
        table_date = datetime(now().year, now().month, now().day - 1, 5, 0)

    data = {"satellites": satellites, "table_date": table_date, "page": "db"}
    return render(request, "base/db.html", data)


def gallery_view(request):
    """
    View to render gallery page
    """
    satellites = (
        Satellite.objects.filter(telemetry_data__is_image=True)
        .values("sat_id", "name")
        .distinct()
    )

    data = {"satellites": satellites}
    return render(request, "base/gallery.html", data)


def satellite_gallery(request, sat_id: str):
    """
    View to render a specific satellite's gallery page

    Args:
        sat_id(str): Sat id of Satellite which images are loading
    """
    satellite = Satellite.objects.only("id", "name").get(sat_id=sat_id)
    observations = (
        Observation.objects.filter(satellite__sat_id=sat_id, demoddata__is_image=True)
        .prefetch_related(
            Prefetch(
                "demoddata",
                queryset=DemodData.objects.filter(is_image=True).only(
                    "id",
                    "demodulated_data",
                    "timestamp",
                    "observation",
                ),
            ),
            "satellite",
            "ground_station",
        )
        .only(
            "id",
            "start",
            "end",
            "satellite__id",
            "satellite__name",
            "satellite__norad_cat_id",
            "ground_station__id",
        )
        .distinct()
    )
    data = {"observations": observations, "satellite": satellite}
    return render(request, "base/satellite_gallery.html", data)


@ajax_required
def gallery_load_images(request, sat_id: str):
    """
    Photo previews for gallery pages. Return responce with\\
    urls of photos

    Args:
        sat_id(str): Sat id of satellite which images loading
    """
    images_all = DemodData.objects.filter(
        satellite__sat_id=sat_id, is_image=True
    ).order_by("-used_in_gallery", "-timestamp")
    try:
        cover = images_all.filter(gallery_cover=True)[0].demodulated_data.url
    except IndexError:
        cover = None

    images: List[str] = []
    for img in images_all:
        if cover and len(images) == 3:
            break
        if cover is None:
            cover = img.demodulated_data.url
            continue
        if len(images) < 2 and img.demodulated_data.url != cover:
            images.append(img.demodulated_data.url)

    data = {"cover": cover, "images": images}

    return JsonResponse(data, safe=False)
