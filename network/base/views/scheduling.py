"""Django base views for SONIKS Network"""

from collections import defaultdict
from datetime import datetime, timedelta
from operator import itemgetter
from typing import Optional

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Count, F, Prefetch, Q
from django.forms import ValidationError
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.timezone import make_aware, now, utc
from django.utils.translation import gettext_lazy as _

from network.api import serializers
from network.base.decorators import ajax_required
from network.base.forms import ObservationFormSet, SatelliteFilterForm
from network.base.models import (
    LatestTleSet,
    Observation,
    Satellite,
    Station,
    Transmitter,
    TransmitterStats,
)
from network.base.perms import schedule_perms, schedule_station_violators_perms
from network.base.scheduling import (
    create_new_observation,
    get_available_stations,
    over_min_duration,
    predict_available_observation_windows,
)
from network.base.serializers import StationSerializer
from network.base.stats import (
    get_satellite_stats_by_transmitter_list,
    transmitters_with_stats,
)
from network.base.validators import (
    NegativeElevationError,
    NoTleSetError,
    ObservationOverlapError,
    OutOfRangeError,
    SchedulingLimitError,
    SinglePassError,
    check_violators_scheduling_limit,
    is_frequency_in_transmitter_range,
    is_transmitter_in_station_range,
)
from network.users.models import User


def create_new_observations(formset, user: User):
    """
    Creates new observations from formset. Error handling is performed by upper layers
    """
    new_observations = []
    observations_per_norad_id = defaultdict(list)
    for observation_data in formset.cleaned_data:
        transmitter = Transmitter.objects.get(uuid=observation_data["transmitter_uuid"])
        center_frequency = observation_data.get("center_frequency", None)
        if transmitter.type == "Transponder" and center_frequency is None:
            center_frequency = (
                transmitter.downlink_high + transmitter.downlink_low
            ) // 2

        observations_per_norad_id[transmitter.satellite.norad_cat_id].append(
            observation_data["start"]
        )

        observation = create_new_observation(
            station=observation_data["ground_station"],
            transmitter=transmitter,
            start=observation_data["start"],
            end=observation_data["end"],
            author=user,
            center_frequency=center_frequency,
        )
        new_observations.append(observation)

    if formset.violators and not user.groups.filter(name="Operators").exists():
        check_violators_scheduling_limit(formset.violators, observations_per_norad_id)

    for observation in new_observations:
        observation.save()

    return new_observations


def observation_new_post(request):
    """
    Handles POST requests for creating one or more new observations
    """
    formset = ObservationFormSet(request.user, request.POST, prefix="obs")
    try:
        if not formset.is_valid():
            errors_list = [error for error in formset.errors if error]
            if errors_list:
                for field in errors_list[0]:
                    messages.error(request, str(errors_list[0][field][0]))
            else:
                messages.error(request, str(formset.non_form_errors()[0]))
            return JsonResponse({"error": _("Ошибка валидации формы")})

        new_observations = create_new_observations(formset, request.user)

        if "scheduled" in request.session:
            del request.session["scheduled"]
        request.session["scheduled"] = list(obs.id for obs in new_observations)

        response = JsonResponse({"ids": list(obs.id for obs in new_observations)})
    except (
        ObservationOverlapError,
        NegativeElevationError,
        NoTleSetError,
        SinglePassError,
        ValidationError,
        ValueError,
        SchedulingLimitError,
    ) as error:
        messages.error(request, str(error))
        response = JsonResponse({"error": str(error)})
    return response


@login_required
def observation_new(request):
    """
    View for new observation
    """
    can_schedule = schedule_perms(request.user)
    if not can_schedule:
        messages.error(request, _("У вас нет прав для планирования наблюдений"))
        return redirect(reverse("base:observations_list"))

    if request.method == "POST":
        return observation_new_post(request)

    satellites = Satellite.objects.filter(status="alive")

    obs_filter = {}
    if request.method == "GET":
        filter_form = SatelliteFilterForm(request.GET)
        if filter_form.is_valid():
            start = filter_form.cleaned_data["start"]
            end = filter_form.cleaned_data["end"]
            ground_station = filter_form.cleaned_data["ground_station"]
            transmitter = filter_form.cleaned_data["transmitter"]
            norad = filter_form.cleaned_data["norad"]

            obs_filter["dates"] = False
            if start and end:  # Either give both dates or ignore if only one is given
                start = datetime.strptime(start, "%Y/%m/%d %H:%M").strftime(
                    "%Y-%m-%d %H:%M"
                )
                end = (
                    datetime.strptime(end, "%Y/%m/%d %H:%M") + timedelta(minutes=1)
                ).strftime("%Y-%m-%d %H:%M")
                obs_filter["start"] = start
                obs_filter["end"] = end
                obs_filter["dates"] = True

            obs_filter["exists"] = True
            if norad:
                obs_filter["norad"] = norad
                obs_filter["transmitter"] = (
                    transmitter  # Add transmitter only if norad exists
                )
            if ground_station:
                obs_filter["ground_station"] = ground_station
        else:
            obs_filter["exists"] = False

    data = {
        "satellites": satellites,
        "obs_filter": obs_filter,
        "date_min_start": settings.OBSERVATION_DATE_MIN_START,
        "date_min_end": settings.OBSERVATION_DATE_MIN_END,
        "date_max_range": settings.OBSERVATION_DATE_MAX_RANGE,
        "warn_min_obs": settings.OBSERVATION_WARN_MIN_OBS,
        "obs_min_duration": settings.OBSERVATION_DURATION_MIN,
        "split": {
            "duration": settings.OBSERVATION_SPLIT_DURATION,
            "break": settings.OBSERVATION_SPLIT_BREAK_DURATION,
        },
    }
    return render(request, "base/observation_new.html", data)


def prediction_windows_parse_parameters(request):
    """
    Parse HTTP parameters with defaults
    """
    params = {
        "sat_norad_id": request.POST["satellite"],
        "transmitter": request.POST["transmitter"],
        "start": make_aware(
            datetime.strptime(request.POST["start"], "%Y-%m-%d %H:%M"), utc
        ),
        "end": make_aware(
            datetime.strptime(request.POST["end"], "%Y-%m-%d %H:%M"), utc
        ),
        "station_ids": request.POST.getlist("stations[]", []),
        "min_horizon": request.POST.get("min_horizon", None),
        "split_duration": int(
            request.POST.get("split_duration", settings.OBSERVATION_SPLIT_DURATION)
        ),
        "break_duration": int(
            request.POST.get(
                "break_duration", settings.OBSERVATION_SPLIT_BREAK_DURATION
            )
        ),
        "overlapped": int(request.POST.get("overlapped", 0)),
        "center_frequency": int(request.POST.get("center_frequency", 0)) or None,
    }

    if params["split_duration"] < 0 or params["break_duration"] < 0:
        raise ValueError(_("Пожалуйста, еще раз проверьте параметры вашего запроса."))
    if not over_min_duration(params["split_duration"]):
        raise ValueError(
            _(
                "Длительность разделения должна превышать минимальную продолжительность наблюдения ({} секунд)."
            ).format(settings.OBSERVATION_DURATION_MIN)
        )

    return params


@ajax_required
def prediction_windows(request):
    """
    Calculates and returns passes of satellites over stations

    TODO: переделать data под словари, а не списки
    """

    try:
        error_found = True
        # Parse and validate parameters
        params = prediction_windows_parse_parameters(request)

        # Check the selected satellite exists and is alive
        satellite = Satellite.objects.filter(status="alive").get(
            norad_cat_id=params["sat_norad_id"]
        )

        # Get TLE set if there is one available for this satellite
        tle = LatestTleSet.objects.get(satellite=satellite).latest

        # Check the selected transmitter exists, and if yes,
        # store this transmitter in the downlink variable
        transmitter = Transmitter.objects.get(uuid=params["transmitter"])
        if not transmitter:
            raise ValueError(_("Вам следует выбрать правильный передатчик."))
        if params["center_frequency"]:
            if not is_frequency_in_transmitter_range(
                params["center_frequency"], transmitter[0]
            ):
                raise OutOfRangeError(
                    _("Центральная частота находится вне диапазона передатчика.")
                )
            downlink = params["center_frequency"]
        if transmitter.type == "Transponder" and not params["center_frequency"]:
            downlink = (transmitter.downlink_high + transmitter.downlink_low) // 2
        else:
            downlink = transmitter.downlink_low
        error_found = False
    except (ValueError, OutOfRangeError) as error:
        data = [{"error": str(error)}]
    except Satellite.DoesNotExist:
        data = [{"error": _("Сначала вам следует выбрать спутник.")}]

    if error_found:
        return JsonResponse(data, safe=False)

    # Fetch all available ground stations
    stations = Station.objects.filter(
        status__gt=0, alt__isnull=False, lat__isnull=False, lng__isnull=False
    ).prefetch_related(
        Prefetch(
            "observations",
            queryset=Observation.objects.filter(end__gt=now()),
            to_attr="scheduled_obs",
        ),
        "antennas",
        "antennas__frequency_ranges",
    )

    if params["station_ids"] and params["station_ids"] != [""]:
        # Filter ground stations based on the given selection
        stations = stations.filter(id__in=params["station_ids"])
        if not stations:
            if len(params["station_ids"]) == 1:
                data = [
                    {
                        "error": (
                            _(
                                "Станция не в сети, она не существует или"
                                " её местоположение не определено."
                            )
                        )
                    }
                ]
            else:
                data = [
                    {
                        "error": (
                            _(
                                "Станции не в сети, они не существуют или"
                                " их местоположение не определено."
                            )
                        )
                    }
                ]
            return JsonResponse(data, safe=False)

    available_stations = get_available_stations(
        stations, downlink, request.user, satellite
    )

    data = []
    passes_found = defaultdict(list)
    for station in available_stations:
        station_passes, station_windows = predict_available_observation_windows(
            station,
            params["min_horizon"],
            params["overlapped"],
            tle,
            params["start"],
            params["end"],
            {"split": params["split_duration"], "break": params["break_duration"]},
        )
        passes_found[station.id] = station_passes
        if station_windows:
            data.append(
                {
                    "id": station.id,
                    "name": station.name,
                    "status": station.status,
                    "lng": station.lng,
                    "lat": station.lat,
                    "alt": station.alt,
                    "window": station_windows,
                }
            )

    if not data:
        error_message = _(
            "Спутник всегда находится ниже горизонта или "
            "на видимых станциях нет свободного времени для наблюдений."
        )
        error_details = {}
        for station in available_stations:
            if station.id not in passes_found:
                error_details[station.id] = _(
                    "Спутник всегда находится выше или ниже горизонта.\n"
                )
            else:
                error_details[station.id] = _(
                    "Нет свободного времени для наблюдения во время пролетов.\n"
                )

        data = [
            {
                "error": error_message,
                "error_details": error_details,
                "passes_found": passes_found,
            }
        ]

    return JsonResponse(data, safe=False)


@ajax_required
def pass_predictions(request, station_id: int, sat_id: Optional[str] = None):
    """
    Endpoint for pass predictions

    Args:
        station_id(int):ID of station which passes we predictf
        sat_id(str,optional):Sat_id of satellite which passes we predict
    Return:
        (JsonResponse):Responce with dict with info about
            station and future passes sorted by start time

    TODO убрать utcnow,адаптировать код под now(timezone.utc)
    """
    station = get_object_or_404(
        Station.objects.prefetch_related(
            Prefetch(
                "observations",
                queryset=Observation.objects.filter(end__gt=now()),
                to_attr="scheduled_obs",
            ),
            "antennas",
            "antennas__frequency_ranges",
        ),
        id=station_id,
        alt__isnull=False,
        lat__isnull=False,
        lng__isnull=False,
    )

    if sat_id:
        satellites = Satellite.objects.filter(status="alive", sat_id=sat_id)
    else:
        satellites = Satellite.objects.filter(status="alive")

    if not schedule_station_violators_perms(request.user, station):
        satellites = satellites.filter(is_frequency_violator="False")

    nextpasses = []
    start = make_aware(datetime.utcnow(), utc)
    end = make_aware(
        datetime.utcnow() + timedelta(hours=settings.STATION_UPCOMING_END), utc
    )
    observation_min_start = (
        datetime.utcnow() + timedelta(minutes=settings.OBSERVATION_DATE_MIN_START)
    ).strftime("%Y-%m-%d %H:%M:%S.%f")
    observation_min_end = (
        datetime.utcnow()
        + timedelta(minutes=settings.OBSERVATION_DATE_MIN_START)
        + timedelta(seconds=settings.OBSERVATION_DURATION_MIN)
    ).strftime("%Y-%m-%d %H:%M:%S.%f")

    for satellite in satellites:
        # look for a match between transmitters from the satellite and
        # ground station antenna frequency capabilities
        try:
            tle = LatestTleSet.objects.filter(satellite=satellite).get().latest
        except LatestTleSet.DoesNotExist:
            continue
        transmitters_all = Transmitter.objects.filter(
            approved=True,
            satellite=satellite,
            status__in=["active", "inactive"],
        )
        transmitters = []
        transmitters_freq = []
        for transmitter in transmitters_all:
            if is_transmitter_in_station_range(transmitter, station):
                transmitters.append(transmitter)
                transmitters_freq.append(transmitter.downlink_low)
        if not transmitters or not tle:
            continue

        _, station_windows = predict_available_observation_windows(
            station,
            None,
            2,
            tle,
            start,
            end,
            {
                "split": settings.OBSERVATION_SPLIT_DURATION,
                "break": settings.OBSERVATION_SPLIT_BREAK_DURATION,
            },
        )

        if station_windows:
            satellite_stats = get_satellite_stats_by_transmitter_list(transmitters)
            for window in station_windows:
                valid = (
                    window["start"] > observation_min_start and window["valid_duration"]
                )
                if not valid:
                    valid = (
                        window["end"] > observation_min_end and window["valid_duration"]
                    )
                window_start = datetime.strptime(
                    window["start"], "%Y-%m-%d %H:%M:%S.%f"
                )
                window_end = datetime.strptime(window["end"], "%Y-%m-%d %H:%M:%S.%f")
                sat_pass = {
                    "name": str(satellite.name),
                    "id": str(satellite.id),
                    "sat_id": str(satellite.sat_id),
                    "success_rate": str(satellite_stats["success_rate"]),
                    "bad_rate": str(satellite_stats["bad_rate"]),
                    "unknown_rate": str(satellite_stats["unknown_rate"]),
                    "future_rate": str(satellite_stats["future_rate"]),
                    "total_count": str(satellite_stats["total_count"]),
                    "good_count": str(satellite_stats["good_count"]),
                    "bad_count": str(satellite_stats["bad_count"]),
                    "unknown_count": str(satellite_stats["unknown_count"]),
                    "future_count": str(satellite_stats["future_count"]),
                    "norad_cat_id": str(satellite.norad_cat_id),
                    "satellite_freq": transmitters_freq,
                    "tle1": window["tle1"],
                    "tle2": window["tle2"],
                    "tr": window_start,  # Rise time
                    "azr": window["az_start"],  # Rise Azimuth
                    "altt": window["elev_max"],  # Max altitude
                    "ts": window_end,  # Set time
                    "azs": window["az_end"],  # Set azimuth
                    "valid": valid,
                    "overlapped": window["overlapped"],
                    "overlap_ratio": window["overlap_ratio"],
                }
                nextpasses.append(sat_pass)

    data = {
        "id": station_id,
        "nextpasses": sorted(nextpasses, key=itemgetter("tr")),
        "ground_station": {
            "lng": str(station.lng),
            "lat": str(station.lat),
            "alt": station.alt,
        },
    }

    return JsonResponse(data, safe=False)


@ajax_required
def scheduling_stations(request):  # pylint: disable=too-many-return-statements
    """
    Returns json with stations on which user has permissions to schedule

    TODO: переделать data под словари, а не списки
    """
    uuid = request.POST.get("transmitter", None)
    if uuid is None:
        data = [{"error": "You should select a Transmitter."}]
        return JsonResponse(data, safe=False)
    try:
        transmitter = Transmitter.objects.get(uuid=uuid)
        if not transmitter:
            data = [{"error": _("Вам следует выбрать правильный передатчик.")}]
            return JsonResponse(data, safe=False)
        downlink = transmitter.downlink_low
        if downlink is None:
            data = [{"error": _("Вам следует выбрать правильный передатчик.")}]
            return JsonResponse(data, safe=False)
        satellite = Satellite.objects.get(
            norad_cat_id=transmitter.satellite.norad_cat_id
        )
    except (Satellite.DoesNotExist, Transmitter.DoesNotExist):
        data = {"error": _("Невозможно найти спутник для выбранного передатчика.")}
        return JsonResponse(data, safe=False)

    stations = (
        Station.objects.filter(
            status__gt=0, alt__isnull=False, lat__isnull=False, lng__isnull=False
        )
        .prefetch_related("antennas", "antennas__frequency_ranges")
        .order_by("id")
    )

    center_frequency = request.POST.get("center_frequency", downlink)
    try:
        center_frequency = int(float(center_frequency))
    except ValueError:
        data = {"error": _("Неверное значение центральной частоты")}
        return JsonResponse(data, safe=False)

    available_stations = get_available_stations(
        stations, center_frequency, request.user, satellite
    )
    data = {
        "stations": StationSerializer(available_stations, many=True).data,
    }
    return JsonResponse(data, safe=False)


def transmitters_list(request):
    """
    Returns a transmitter JSON object with information and statistics

    TODO: переписать data всех jsonResponce с списка на словари
    """
    norad_id = request.GET.get("satellite", None)
    station_id = request.GET.get("station_id", None)
    try:
        if norad_id:
            satellite = Satellite.objects.get(norad_cat_id=norad_id)
        else:
            data = {"error": _("Спутник не предоставлен.")}
            return JsonResponse(data, safe=False)
    except Satellite.DoesNotExist:
        data = {"error": _("Не удалось найти этот спутник.")}
        return JsonResponse(data, safe=False)

    transmitters = (
        Transmitter.objects.filter(satellite=satellite)
        .exclude(status="invalid", downlink_low=None)
        .prefetch_related("stat")
        .annotate(
            future_count=Count("observations", filter=Q(observations__end__gt=now()))
        )
    )
    if len(transmitters) == 0:
        data = [{"error": _("Передатчики не найдены.")}]
        return JsonResponse(data, safe=False)

    if station_id:
        supported_transmitters = []
        try:
            station = Station.objects.prefetch_related(
                "antennas", "antennas__frequency_ranges"
            ).get(id=station_id)
        except Station.DoesNotExist:
            data = {"error": _("Не удалось найти эту станцию.")}
            return JsonResponse(data, safe=False)

        if satellite.is_frequency_violator and not schedule_station_violators_perms(
            request.user, station
        ):
            data = {
                "error": _(
                    "Нет разрешения на планирование этого спутника на этой станции."
                )
            }
            return JsonResponse(data, safe=False)
        for transmitter in transmitters:
            transmitter_supported = is_transmitter_in_station_range(
                transmitter, station
            )
            if transmitter_supported:
                supported_transmitters.append(transmitter)
        transmitters = supported_transmitters
    transmitters = serializers.TransmitterSerializer(transmitters, many=True).data
    data = {
        "transmitters_active": transmitters_with_stats(
            [
                t
                for t in transmitters
                if t["status"] == "active" and not t["unconfirmed"]
            ]
        ),
        "transmitters_inactive": transmitters_with_stats(
            [
                t
                for t in transmitters
                if t["status"] == "inactive" and not t["unconfirmed"]
            ]
        ),
        "transmitters_unconfirmed": transmitters_with_stats(
            [t for t in transmitters if t["unconfirmed"]]
        ),
    }

    return JsonResponse(data, safe=False)
