"""Django base views for SONIKS Network"""

from bootstrap_modal_forms.generic import BSModalCreateView
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView

from network.base.forms import TransmitterEditForm
from network.base.models import Mode, Satellite, Transmitter


class TransmitterListView(ListView):  # pylint: disable=R0901
    """
    Displays a list of transmitters
    """

    model = Transmitter
    context_object_name = "transmitters"
    template_name = "base/transmitters.html"
    flag_filters = ["active", "inactive"]
    flag_type = [_("Передатчик"), _("Приемник"), _("Приемопередатчик")]
    name_filters = [
        "type",
        "mode",
        "satellite",
        "description",
        "baud-start",
        "baud-end",
        "downlink-start",
        "downlink-end",
    ]
    more_filtered = None

    def get_filter_params(self):
        """
        Get the parsed filter parameters from the HTTP GET parameters
        """
        filter_params = {}
        for param_name in self.name_filters:
            filter_params[param_name] = self.request.GET.get(param_name, "")
        for param_name in self.flag_filters:
            param = self.request.GET.get(param_name, 1)
            filter_params[param_name] = param != "0"
        return filter_params

    def get_queryset(self):
        parameter_filter_mapping = {
            "type": "type",
            "mode": "downlink_mode__id",
            "satellite": "satellite__sat_id",
            "description": "description__icontains",
            "baud-start": "baud__gte",
            "baud-end": "baud__lte",
            "downlink-start": "downlink_low__gte",
            "downlink-end": "downlink_low__lte",
        }
        filter_params = self.get_filter_params()
        filter_dict = {}
        for parameter_key, filter_key in parameter_filter_mapping.items():
            if filter_params[parameter_key] == "":
                continue
            filter_dict[filter_key] = filter_params[parameter_key]
        transmitters = Transmitter.objects.prefetch_related(
            "satellite", "downlink_mode", "uplink_mode"
        )
        transmitters = transmitters.filter(**filter_dict).exclude(approved=False)
        for i in self.flag_filters:
            if not filter_params[i]:
                transmitters = transmitters.exclude(status=i)

        self.more_filtered = (
            filter_dict.get(
                "description__icontains",
            )
            or filter_dict.get("baud__gte")
            or filter_dict.get(
                "baud__lte",
            )
            or filter_dict.get("downlink_low__gte")
            or filter_dict.get("downlink_low__lte")
        )

        return transmitters

    def get_display_columns(self):
        """
        Keep columns displayed when refreshing page
        """
        columns_list = [
            "T_Satellite",
            "T_Description",
            "T_Type",
            "T_Downlink",
            "T_Mode",
            "T_Status",
            "T_Uuid",
            "T_Baud",
            "T_Спутник",
            "T_Описание",
            "T_Тип",
            "T_Частота",
            "T_Модуляция",
            "T_Статус",
            "T_Скорость",
            "T_Изменить",
        ]
        columns_display = {}
        for column in columns_list:
            columns_display[column] = self.request.COOKIES.get(column)
        return columns_display

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["active"] = self.request.GET.get("active", "1")
        context["inactive"] = self.request.GET.get("inactive", "1")
        context["selected_type"] = self.request.GET.get("type", None)
        context["all_type"] = self.flag_type
        context["columns"] = self.get_display_columns()
        context["modes"] = Mode.objects.all()
        context["satellites"] = Satellite.objects.all()
        selected_mode = self.request.GET.get("mode", None)
        if selected_mode is not None and selected_mode != "":
            context["selected_mode"] = int(selected_mode)
        context["selected_satellite"] = self.request.GET.get("satellite", None)
        context["description"] = self.request.GET.get("description", None)
        context["baud_min"] = self.request.GET.get("baud-start", None)
        context["baud_max"] = self.request.GET.get("baud-end", None)
        context["downlink_min"] = self.request.GET.get("downlink-start", None)
        context["downlink_max"] = self.request.GET.get("downlink-end", None)
        context["more_filtered"] = bool(self.more_filtered)
        context["page"] = "transmitters"

        return context


class TransmitterEditView(LoginRequiredMixin, BSModalCreateView):
    """
    Displaying the transmitter edit form
    """

    template_name = "base/modals/edit_transmitter.html"
    model = Transmitter
    form_class = TransmitterEditForm
    success_message = _(
        "наше предложение по передатчику было успешно сохранено и будет \n проверено модератором. Спасибо за вклад!"
    )

    error_message = _("Форма недействительна")

    user = get_user_model()

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        transmitter = form.instance

        try:
            transmitter_obj = Transmitter.objects.get(uuid=transmitter.uuid_by_changes)
            transmitter.satellite_id = transmitter_obj.satellite_id
            transmitter.approved = False
            return super().form_valid(form)
        except Transmitter.DoesNotExist:
            messages.error(self.request, self.error_message)
            return redirect(self.request.META.get("HTTP_REFERER"))

    def get_success_url(self):
        return self.request.META.get("HTTP_REFERER")
