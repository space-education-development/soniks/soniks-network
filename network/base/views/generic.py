"""Django base views for SONIKS Network"""

from urllib.parse import urlencode

from django.conf import settings
from django.contrib import messages
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse

from network.base.decorators import staff_required
from network.base.models import NetworkSchedule, Satellite, Station, Transmitter
from network.base.tasks import fetch_data, update_future_observations_with_new_tle_sets
from network.base.utils import format_frequency, get_dict_from_list


def index(request):
    """
    View to render index page
    """
    data = {
        "mapbox_id": settings.MAPBOX_MAP_ID,
        "mapbox_token": settings.MAPBOX_TOKEN,
        "satellites_count": Satellite.objects.count(),
        "stations_count": Station.objects.count(),
    }

    return render(request, "base/home.html", data)


def robots(request):
    """
    Returns response for robots.txt requests
    """
    data = render(request, "robots.txt", {"environment": settings.ENVIRONMENT})
    response = HttpResponse(data, content_type="text/plain; charset=utf-8")
    return response


@staff_required
def settings_site(request):
    """
    View to render settings page
    """
    if request.method == "POST":
        fetch_data.delay()
        update_future_observations_with_new_tle_sets.delay()
        messages.success(request, "Data fetching task was triggered successfully!")
        return redirect(
            reverse("users:view_user", kwargs={"username": request.user.username})
        )
    satellites = Satellite.objects.filter(status="alive").exclude(
        is_frequency_violator=True
    )

    try:
        network_schedule = NetworkSchedule.objects.get(id=1)
        network_schedule_list = network_schedule.satellites
        network_schedule_satellites = Satellite.objects.filter(
            sat_id__in=[sat.get("sat_id", None) for sat in network_schedule_list]
        ).values("id", "name", "norad_cat_id", "sat_id")
        network_schedule_transmitters = Transmitter.objects.filter(
            uuid__in=[
                sat.get("transmitter_uuid", None) for sat in network_schedule_list
            ]
        ).values("uuid", "downlink_low", "downlink_mode__name", "type")
        for one_sat in network_schedule_list:
            one_sat_transmitter = get_dict_from_list(
                network_schedule_transmitters,
                "uuid",
                one_sat["transmitter_uuid"],
            )
            one_sat_satellite = get_dict_from_list(
                network_schedule_satellites, "sat_id", one_sat["sat_id"]
            )
            one_sat["norad_cat_id"] = one_sat_satellite["norad_cat_id"]
            one_sat["transmitter_freq"] = format_frequency(
                one_sat_transmitter["downlink_low"]
            )
            one_sat["transmitter_mode"] = one_sat_transmitter["downlink_mode__name"]
    except Exception:
        network_schedule = {}
        network_schedule_list = {}

    return render(
        request,
        "base/settings_site.html",
        {
            "satellites": satellites,
            "network_schedule": network_schedule,
            "network_schedule_list": network_schedule_list,
            "obs_min_duration": settings.OBSERVATION_DURATION_MIN,
            "split": {
                "duration": settings.OBSERVATION_SPLIT_DURATION,
                "break": settings.OBSERVATION_SPLIT_BREAK_DURATION,
            },
        },
    )


def spectrum(request):
    """
    View to render spectrum page.
    """
    data = {"page": "spectrum"}
    return render(request, "base/spectrum.html", data)


def about(request):
    """View to render about page."""
    data = {"page": "about"}
    return render(request, "base/about.html", data)


def space_education(request):
    """View to render 'Development of space education' page."""

    return render(request, "base/partners/space_education.html")


def space_education_redirect(request):
    """Redirect to 'Development of space education' page."""

    return redirect("base:space_education")


def provider_logout(request):
    """
    Create the user's OIDC logout URL
    """
    # User must confirm logout request with the default logout URL
    # and is not redirected.
    logout_url = settings.OIDC_OP_LOGOUT_ENDPOINT

    # If we have the oidc_id_token, we can automatically redirect
    # the user back to the application.
    oidc_id_token = request.session.get("oidc_id_token", None)
    if oidc_id_token:
        logout_url = (
            settings.OIDC_OP_LOGOUT_ENDPOINT
            + "?"
            + urlencode(
                {
                    "id_token_hint": oidc_id_token,
                    "post_logout_redirect_uri": request.build_absolute_uri(
                        location=settings.LOGOUT_REDIRECT_URL
                    ),
                    "returnTo": logout_url,
                }
            )
        )

    return logout_url


def education(request):
    """View to render education page."""
    data = {"page": "education"}
    return render(request, "base/education.html", data)


def openid_logout(request):
    """
    Create for logout from OpenID provider and Django acc together
    """
    return render(request, "account/logout_openid.html")
