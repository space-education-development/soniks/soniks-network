"""SONIKS Network scheduling functions"""

# ephem is missing lon, lat, elevation and horizon attributes in Observer class slots,
# Disable assigning-non-slot pylint error:
# pylint: disable=E0237
import math
from datetime import datetime, timedelta, timezone
from typing import Dict, List, Optional, Union

import ephem
from django.conf import settings
from django.db.models.query import QuerySet
from django.utils.timezone import make_aware, now

from network.base.models import (
    LatestTleSet,
    Observation,
    Satellite,
    Station,
    Tle,
    Transmitter,
)
from network.base.perms import schedule_stations_perms
from network.base.utils import format_frequency
from network.base.validators import (
    NegativeElevationError,
    ObservationOverlapError,
    OutOfRangeError,
    SinglePassError,
)
from network.users.models import User

# ephem.Observer describe Station model as physical object
# ephem.EarthSatellite describe Satellite model as physical object


def get_altitude(
    observer: ephem.Observer, satellite: ephem.EarthSatellite, date: datetime
):
    """
    Returns altitude of satellite in a specific date for a specific observer

    Args:
        observer(ephem.Observer): Station above which altitude counting
        satellite(ephem.EarthSatellite): Satellite which altutide couting
        date(datetime): Date when altutide couting

    Returns:
        float: Altutide of satellite above target observer
    """
    old_date = observer.date
    observer.date = date
    satellite.compute(observer)
    observer.date = old_date
    return float(format(math.degrees(satellite.alt), ".0f"))


def get_azimuth(
    observer: ephem.Observer, satellite: ephem.EarthSatellite, date: datetime
):
    """
    Returns azimuth of satellite in a specific date for a specific observer
    Args:
        observer(ephem.Observer): Station above which azimuth counting
        satellite(ephem.EarthSatellite): Satellite whichh azimuth couting
        date(datetime): Date when azimuth counting

    Returns:
        float: Azimuth of satellite above target observer

    *Нигде не используется
    """
    old_date = observer.date
    observer.date = date
    satellite.compute(observer)
    observer.date = old_date
    return float(format(math.degrees(satellite.az), ".0f"))


def over_min_duration(duration: float) -> bool:
    """
    Returns if duration is equal or greater than the minimum one set in settings

    Args:
        duration(float): Duration of observation window

    Returns:
        bool: True if duration longer then minumum duration else False
    """
    return duration >= settings.OBSERVATION_DURATION_MIN


def recalculate_window_parameters(
    observer: ephem.Observer,
    satellite: ephem.EarthSatellite,
    window_start: datetime,
    window_end: datetime,
):
    """
    Finds the start azumith,end azimuth and maximum altitude \\
    of a satellite during a certain observation window
    Args:
        observer(ephem.Observer): Station above which window recalculated
        satellite(ephem.EarthSatellite): Satellite whichh window recalculated
        window_start(datetime): Window start time
        window_end(datetime): Window end time

    Returns:
        turple: Turple with start azimuth,end azimuth and max altutide values
    """
    old_date = observer.date
    satellite = satellite.copy()
    observer.date = window_end
    satellite.compute(observer)
    end_azimuth = float(format(math.degrees(satellite.az), ".0f"))
    observer.date = window_start
    satellite.compute(observer)
    start_azimuth = float(format(math.degrees(satellite.az), ".0f"))

    interval = 1  # in seconds
    max_altitude = 0.0
    date = window_start
    while date < window_end:
        observer.date = date
        satellite.compute(observer)
        altitude = float(format(math.degrees(satellite.alt), ".0f"))
        max_altitude = max(altitude, max_altitude)
        date = date + timedelta(seconds=interval)
    observer.date = old_date

    return (start_azimuth, end_azimuth, max_altitude)


def split_long_window(
    start: datetime,
    end: datetime,
    duration: float,
    split_duration: int,
    break_duration: int,
):
    """
    Split long passes into 'split_duration' seconds ones and \\
    let between them a period of 'break_duration' seconds

    Args:
        start(datetime): Start of window for splitting
        end(datetime): End of window for splitting
        duration(float): Duration of window for splitting
        split_duration(int): Duration of one splitted window
        break_duration(int): Pause between splitted windows

    Returns:
        list: List of splitted windows from long window
    """
    windows = []
    split_number = (int(duration) // (split_duration + break_duration)) + 1
    total_splits = split_number

    last_split_duration = duration % (split_duration + break_duration)
    if not over_min_duration(last_split_duration):
        total_splits -= 1

    for split in range(0, total_splits):
        start_offset = split * (split_duration + break_duration)
        window_start = start + timedelta(seconds=start_offset)
        if total_splits - split == 1:  # last split
            window_end = end
        else:
            window_end = window_start + timedelta(seconds=split_duration)
        windows.append({"start": window_start, "end": window_end})

    return windows


def resolve_overlaps(scheduled_obs: QuerySet, start: datetime, end: datetime):
    """
    This function checks for overlaps between all existing observations on `scheduled_obs`\\
    and a potential new observation with given `start` and `end` time

    Args:
        scheduled_obs(QuerySet): All future observation of station
        start(datetime): Start time of potential new observation
        end(datetime): End time of potential new observation

    Returns:
        turple: Turple with variants:
        - ([], True)                                  if total overlap exists
        - ([(start1, end1), (start2, end2)], True)    if the overlap happens in the middle
                                                    of the new observation
        - ([(start, end)], True)                      if the overlap happens at one end
                                                    of the new observation
        - ([(start, end)], False)                     if no overlap exists
    """
    overlapped = False
    if scheduled_obs:
        for datum in scheduled_obs:
            if datum.start <= end and start <= datum.end:
                overlapped = True
                if datum.start <= start and datum.end >= end:
                    return ([], True)
                if start < datum.start and end > datum.end:
                    # In case of splitting the window  to two we
                    # check for overlaps for each generated window.
                    window1 = resolve_overlaps(
                        scheduled_obs, start, datum.start - timedelta(seconds=30)
                    )
                    window2 = resolve_overlaps(
                        scheduled_obs, datum.end + timedelta(seconds=30), end
                    )
                    return (window1[0] + window2[0], True)
                if datum.start <= start:
                    start = datum.end + timedelta(seconds=30)
                if datum.end >= end:
                    end = datum.start - timedelta(seconds=30)
    return ([(start, end)], overlapped)


def create_station_window(
    window_start: datetime,
    window_end: datetime,
    azr: float,
    azs: float,
    altitude: float,
    tle: Tle,
    valid_duration: bool,
    overlapped: bool,
    split: bool,
    overlap_ratio: int = 0,
):
    """
    Creates an observation window

    Args:
        window_start(datetime): Start of window
        window_end(datetime): End of window
        azr(float): Start azimuth of window
        azs(float): End azimuth of window
        altitude(float): Max altitude during window
        tle(Tle): Instance of Tle used for this observation
        valid_duration(bool): True if duration is valid Else False
        overlapped(bool): True if overlap exists else False
        split(bool): True if window was splitted else False
        overlap_ratio(int): Overlap percentage ratio

    Returns:
        dict: Single window dict
    """
    return {
        "start": window_start.strftime("%Y-%m-%d %H:%M:%S.%f"),
        "end": window_end.strftime("%Y-%m-%d %H:%M:%S.%f"),
        "az_start": azr,
        "az_end": azs,
        "elev_max": altitude,
        "tle0": tle.tle0,
        "tle1": tle.tle1,
        "tle2": tle.tle2,
        "valid_duration": valid_duration,
        "overlapped": overlapped,
        "split": split,
        "overlap_ratio": overlap_ratio,
    }


def create_station_windows(
    scheduled_obs: QuerySet,
    overlapped: int,
    pass_params: dict,
    observer: ephem.Observer,
    satellite: ephem.EarthSatellite,
    tle: Tle,
    duration: Optional[dict] = None,
):
    """
    This function takes a pre-calculated pass (described by pass_params) over a certain \\
    station and a list of already scheduled observations, and calculates observation \\
    windows during whichh the station is available to observe the pass

    Args:
        scheduled_obs(Queryset): Queryset of all future observation of station.
            Passed with station andcalculated when quering db
        overlapped(int): Parsed parameter, describe overlap behavior
        pass_params(dict): Dict with params from pyephem.next_pass,
            describe next pass of satellite above station
        observer(ephem.Observer): Station above which pass windows are calculating
        satellite(ephem.EarthSatellite): Satellite which pass windows are calculating
        tle(Tle): Instance of Tle(probably latest) for this satellite
        duration(dict): Dict with two params describing custom break_duration
            and split duration. If None use default params from settings
    Returns:
        list: List of all available observation windows
    """
    station_windows: List[dict] = []

    if not duration:
        duration = {
            "split": settings.OBSERVATION_SPLIT_DURATION,
            "break": settings.OBSERVATION_SPLIT_BREAK_DURATION,
        }
    windows, windows_changed = resolve_overlaps(
        scheduled_obs, pass_params["rise_time"], pass_params["set_time"]
    )

    if not windows:
        # No overlapping windows found
        return []
    if windows_changed:
        # Windows changed due to overlap, recalculate observation parameters
        if overlapped == 0:
            return []

        if overlapped == 1:
            initial_duration = (
                pass_params["set_time"] - pass_params["rise_time"]
            ).total_seconds()
            for window_start, window_end in windows:
                window_duration = (window_end - window_start).total_seconds()
                if not over_min_duration(window_duration):
                    continue

                if window_duration > duration["split"]:
                    split_windows = split_long_window(
                        window_start,
                        window_end,
                        window_duration,
                        duration["split"],
                        duration["break"],
                    )
                    for split_window in split_windows:
                        # Add windows for a partial split passes
                        start_azimuth, end_azimuth, max_altitude = (
                            recalculate_window_parameters(
                                observer,
                                satellite,
                                split_window["start"],
                                split_window["end"],
                            )
                        )
                        station_windows.append(
                            create_station_window(
                                split_window["start"],
                                split_window["end"],
                                start_azimuth,
                                end_azimuth,
                                max_altitude,
                                tle,
                                True,
                                True,
                                True,
                                min(1, 1 - (window_duration / initial_duration)),
                            )
                        )
                else:
                    # Add a window for a partial pass
                    start_azimuth, end_azimuth, max_altitude = (
                        recalculate_window_parameters(
                            observer, satellite, window_start, window_end
                        )
                    )
                    station_windows.append(
                        create_station_window(
                            window_start,
                            window_end,
                            start_azimuth,
                            end_azimuth,
                            max_altitude,
                            tle,
                            True,
                            True,
                            False,
                            min(1, 1 - (window_duration / initial_duration)),
                        )
                    )
        elif overlapped == 2:
            initial_duration = (
                pass_params["set_time"] - pass_params["rise_time"]
            ).total_seconds()
            total_window_duration = 0
            window_duration = 0
            duration_validity = False
            for window_start, window_end in windows:
                window_duration = (window_end - window_start).total_seconds()
                duration_validity = duration_validity or over_min_duration(
                    window_duration
                )
                total_window_duration += window_duration

            # If duration is longer than 12min then satellite is probably on higher than LEO orbit
            # and we need to recalculate pass parameters
            if initial_duration > 720:
                start_azimuth, end_azimuth, max_altitude = (
                    recalculate_window_parameters(
                        observer,
                        satellite,
                        pass_params["rise_time"],
                        pass_params["set_time"],
                    )
                )
            else:
                start_azimuth, end_azimuth, max_altitude = (
                    pass_params["rise_az"],
                    pass_params["set_az"],
                    pass_params["tca_alt"],
                )
            # Add a window for the overlapped pass, this is for station page and will not be split
            # as we want it as one. The split will be done when user click on schedule button for
            # this observation and it will be moved to observation/new page.
            station_windows.append(
                create_station_window(
                    pass_params["rise_time"],
                    pass_params["set_time"],
                    start_azimuth,
                    end_azimuth,
                    max_altitude,
                    tle,
                    duration_validity,
                    True,
                    False,
                    min(1, 1 - (total_window_duration / initial_duration)),
                )
            )
    else:
        window_duration = (windows[0][1] - windows[0][0]).total_seconds()
        if over_min_duration(window_duration):
            # if overlapped == 2 then the pass is presented in station page, in this case pass
            # should be kept without splitting
            if window_duration > duration["split"] and overlapped != 2:
                split_windows = split_long_window(
                    windows[0][0],
                    windows[0][1],
                    window_duration,
                    duration["split"],
                    duration["break"],
                )
                for split_window in split_windows:
                    # Add windows for a partial split passes
                    start_azimuth, end_azimuth, max_altitude = (
                        recalculate_window_parameters(
                            observer,
                            satellite,
                            split_window["start"],
                            split_window["end"],
                        )
                    )
                    station_windows.append(
                        create_station_window(
                            split_window["start"],
                            split_window["end"],
                            start_azimuth,
                            end_azimuth,
                            max_altitude,
                            tle,
                            True,
                            False,
                            True,
                            0,
                        )
                    )
            else:
                # If duration is longer than 12min then satellite is probably on higher than LEO
                # orbit and we need to recalculate pass parameters
                if window_duration > 720:
                    start_azimuth, end_azimuth, max_altitude = (
                        recalculate_window_parameters(
                            observer,
                            satellite,
                            pass_params["rise_time"],
                            pass_params["set_time"],
                        )
                    )
                else:
                    start_azimuth, end_azimuth, max_altitude = (
                        pass_params["rise_az"],
                        pass_params["set_az"],
                        pass_params["tca_alt"],
                    )
                # Add a window for a full pass
                station_windows.append(
                    create_station_window(
                        pass_params["rise_time"],
                        pass_params["set_time"],
                        start_azimuth,
                        end_azimuth,
                        max_altitude,
                        tle,
                        True,
                        False,
                        False,
                        0,
                    )
                )
    return station_windows


def next_pass(
    observer: ephem.Observer, satellite: ephem.EarthSatellite, singlepass: bool = True
):
    """
    Returns the next pass of the satellite above the observer

    Args:
        observer(ephem.Observer): Station above which pass are calculating
        satellite(ephem.EarthSatellite): Satellite which pass are calculating
        singlepass(bool): Parameter needed for pyemhem.Observer.next_pass func.
            Used as False only when catching SinglePassError in create_new_observation

    Returns:
        dict: Dict with six pass params as keys and their valur as values
    """
    rise_time, rise_az, tca_time, tca_alt, set_time, set_az = observer.next_pass(
        satellite, singlepass
    )
    # Convert output of pyephems.next_pass into processible values
    pass_start = make_aware(ephem.Date(rise_time).datetime(), timezone.utc)
    pass_end = make_aware(ephem.Date(set_time).datetime(), timezone.utc)
    pass_tca = make_aware(ephem.Date(tca_time).datetime(), timezone.utc)
    pass_azr = float(format(math.degrees(rise_az), ".0f"))
    pass_azs = float(format(math.degrees(set_az), ".0f"))
    pass_altitude = float(format(math.degrees(tca_alt), ".0f"))

    return {
        "rise_time": pass_start,
        "set_time": pass_end,
        "tca_time": pass_tca,
        "rise_az": pass_azr,
        "set_az": pass_azs,
        "tca_alt": pass_altitude,
    }


def generate_geo_observation_window(
    observer: ephem.Observer,
    satellite: ephem.EarthSatellite,
    start: datetime,
    end: datetime,
):
    """
    Calculate a pass for an object already overhead for satellite with\\
    geostationary orbit

    Args:
        observer(ephem.Observer): Station above which pass are calculating
        satellite(ephem.EarthSatellite): Satellite which pass are calculating
        start(datetime): Start of scheduling period
        end(datetime): End tile of scheduling period

    Returns:
        dict: Dict with params for geo obs window

    """
    pass_params: Dict[str, Union[datetime, int, str]] = {}
    try:
        satellite.compute(observer)
    except ValueError:
        return pass_params
    pass_params["rise_time"] = start
    pass_params["rise_az"] = int(satellite.az * 180 / math.pi)
    pass_params["tca_time"] = (start + timedelta(hours=12)).strftime(
        "%Y-%m-%d %H:%M:%S.%f"
    )
    pass_params["tca_alt"] = int(satellite.alt * 180 / math.pi)
    # skip to end of period and take 'set' measurements
    observer.date = ephem.Date(observer.date + 24 * ephem.hour)
    satellite.compute(observer)
    pass_params["set_time"] = end
    pass_params["set_az"] = int(satellite.az * 180 / math.pi)
    return pass_params


def generate_overhead_observation_window(
    observer: ephem.Observer, satellite: ephem.EarthSatellite
):
    """
    Calculate a pass for an object already overhead

    Args:
        observer(ephem.Observer): Station above which pass are calculating
        satellite(ephem.EarthSatellite): Satellite which pass are calculating

    Returns:
        dict: Dict with pass parameters used for generating observation windows
    """
    pass_params: Dict[str, Union[datetime, int, str]] = {}
    min_horizon = float(observer.horizon)
    satellite.compute(observer)
    # Window is up, rise time is now
    pass_params["rise_time"] = make_aware(observer.date.datetime(), timezone.utc)
    pass_params["rise_az"] = int(satellite.az * 180 / math.pi)
    max_alt = satellite.az
    max_alt_time = observer.date
    seconds_for_finding_rise = 0
    # Search forward to set time, and catch tca during search
    while (
        ephem.degrees(satellite.alt) > min_horizon
        and seconds_for_finding_rise < 60 * 24
    ):
        if max_alt < satellite.alt:
            max_alt = satellite.alt
            max_alt_time = observer.date
        observer.date = ephem.Date(observer.date + ephem.second)
        seconds_for_finding_rise += 1
        satellite.compute(observer)
    # observation_min_end = (
    #     now() + timedelta(minutes=settings.OBSERVATION_DATE_MIN_START) +
    #     timedelta(seconds=settings.OBSERVATION_DURATION_MIN)
    # ).strftime("%Y-%m-%d %H:%M:%S.%f")
    # if observer.date.datetime() < observation_min_end:
    #     return {}
    # Found set time
    pass_params["set_time"] = make_aware(observer.date.datetime(), timezone.utc)
    pass_params["set_az"] = int(satellite.az * 180 / math.pi)
    # Move to TCA
    observer.date = max_alt_time
    satellite.compute(observer)
    pass_params["tca_time"] = make_aware(observer.date.datetime(), timezone.utc)
    pass_params["tca_alt"] = int(satellite.alt * 180 / math.pi)
    return pass_params


def predict_available_observation_windows(
    station: Station,
    min_horizon: Optional[int],
    overlapped: int,
    tle: Tle,
    start: datetime,
    end: datetime,
    duration: dict,
):
    """
    Calculate available observation windows for a certain station and satellite during\\
    the given time period.

    Args:
        station(Station): Instance of Station for predicting
        min_horizon(int): Overwrite station minimum horizon if defined
        overlapped(int): Calculate and return overlapped observations fully, truncated or
            not at all.Integer values: 0 (no return), 1(truncated overlaps), 2(full overlaps)
        tle(Tle): Instance of latest Satellite Tle
        start(datetime): Start datetime of scheduling period
        end(datetime): End datetime of scheduling period
        duration(dict): Dict with two params describing break_duration
            and split duration

    Returns:
        turple: Turple with two lists: list of passed
        found and list of available observation windows
    """
    passes_found: List[dict] = []
    station_windows: List[dict] = []
    # Initialize pyehem Satellite for propagation
    satellite = ephem.readtle(str(tle.tle0), str(tle.tle1), str(tle.tle2))
    # Initialize pyephem Observer for propagation
    observer = ephem.Observer()
    observer.lon = str(station.lng)
    observer.lat = str(station.lat)
    observer.elevation = station.alt
    observer.date = ephem.Date(start)
    # Speeds up calculations by removing refraction
    observer.pressure = 0
    if min_horizon is not None:
        observer.horizon = str(min_horizon)
    else:
        observer.horizon = str(station.horizon)

    try:
        try:
            satellite.compute(observer)
        except ValueError:
            return passes_found, station_windows

        # satellite currently up
        if ephem.degrees(satellite.alt) > float(observer.horizon):
            try:
                # Will cause GEO to error out, HEO & LEO caught here
                geo_pass = False
                pass_params = next_pass(observer, satellite)
                # Discard previous results, generate window from satellite overhead
                pass_params = generate_overhead_observation_window(observer, satellite)
            # GEO caught here
            except ValueError:
                pass_params = generate_geo_observation_window(
                    observer, satellite, start, end
                )
                if pass_params:
                    geo_pass = True
                else:
                    return passes_found, station_windows
            except TypeError:
                return passes_found, station_windows
            passes_found.append(pass_params)
            # Check if overlaps with existing scheduled observations
            # Adjust or discard window if overlaps exist
            scheduled_obs = station.scheduled_obs

            station_windows.extend(
                create_station_windows(
                    scheduled_obs,
                    overlapped,
                    pass_params,
                    observer,
                    satellite,
                    tle,
                    duration,
                )
            )
            time_start_new = pass_params["set_time"] + timedelta(minutes=1)
            observer.date = time_start_new.strftime("%Y-%m-%d %H:%M:%S.%f")
            if geo_pass:
                return passes_found, station_windows

    except RuntimeError:
        return passes_found, station_windows

    while True:
        try:
            pass_params = next_pass(observer, satellite)
        # We catch TypeError, to avoid cases like this one that is described in ephem issue:
        # https://github.com/brandon-rhodes/pyephem/issues/176
        except (TypeError, ValueError):
            break

        # no match if the sat will not rise above the configured min horizon
        if pass_params["rise_time"] >= end:
            # start of next pass outside of window bounds
            break

        if pass_params["set_time"] > end:
            # end of next pass outside of window bounds
            pass_params["set_time"] = end

        passes_found.append(pass_params)
        # Check if overlaps with existing scheduled observations
        # Adjust or discard window if overlaps exist
        scheduled_obs = station.scheduled_obs

        station_windows.extend(
            create_station_windows(
                scheduled_obs,
                overlapped,
                pass_params,
                observer,
                satellite,
                tle,
                duration,
            )
        )
        time_start_new = pass_params["set_time"] + timedelta(minutes=1)
        observer.date = time_start_new.strftime("%Y-%m-%d %H:%M:%S.%f")
    return passes_found, station_windows


def create_new_observation(
    station: Station,
    transmitter: Transmitter,
    start: datetime,
    end: datetime,
    author: User,
    center_frequency: Optional[float] = None,
    is_auto: bool = False,
):
    """
    Creates and returns a new Observation object

    Args:
        station(Station): Instance of Station model
        transmitter(Transmitter): Instance of Transmitter model
        start(datetime): Start time of observation
        end(datetime): End time of observation
        author(User): Instance of User who creating observation
        center_frequency(float): center frequency of observation
        is_auto(bool): True if observation created by autoplanner.
            Default is False

    Returns:
        network.base.models.Observation: New instance of Observation model

    Raises:
        NegativeElevationError, ObservationOverlapError, SinglePassError or more
    """
    scheduled_obs = Observation.objects.filter(ground_station=station).filter(
        end__gt=now()
    )
    window = resolve_overlaps(scheduled_obs, start, end)

    if window[1]:
        raise ObservationOverlapError(
            "One or more observations of station {0} overlap with the already scheduled ones.".format(
                station.id
            )
        )

    sat = transmitter.satellite
    tle_latest = LatestTleSet.objects.get(satellite=sat).latest

    sat_ephem = ephem.readtle(
        str(tle_latest.tle0), str(tle_latest.tle1), str(tle_latest.tle2)
    )
    observer = ephem.Observer()
    observer.lon = str(station.lng)
    observer.lat = str(station.lat)
    observer.elevation = station.alt

    rise_azimuth, set_azimuth, max_altitude = recalculate_window_parameters(
        observer, sat_ephem, start, end
    )
    rise_altitude = get_altitude(observer, sat_ephem, start)
    set_altitude = get_altitude(observer, sat_ephem, end)

    if rise_altitude < 0:
        raise NegativeElevationError(
            "Satellite with transmitter {} has negative altitude ({})"
            " for station {} at start datetime: {}".format(
                transmitter.uuid, rise_altitude, station.id, start
            )
        )
    if set_altitude < 0:
        raise NegativeElevationError(
            "Satellite with transmitter {} has negative altitude ({})"
            " for station {} at end datetime: {}".format(
                transmitter.uuid, set_altitude, station.id, end
            )
        )
    # Using a short time (1min later) after start for finding the next pass of the satellite to
    # check that end datetime is before the start datetime of the next pass, in other words that
    # end time belongs to the same single pass.
    observer.date = start + timedelta(minutes=1)
    try:
        next_satellite_pass = next_pass(observer, sat_ephem, False)
        if next_satellite_pass["rise_time"] < end:
            raise SinglePassError(
                "Observation should include only one pass of the satellite with transmitter {}"
                " on station {}, please check start({}) and end({}) datetimes and try again".format(
                    transmitter.uuid, station.id, start, end
                )
            )
    # not valid for always up
    except ValueError:
        pass

    # List all station antennas with their frequency ranges.
    is_center_frequency_in_station_range = False
    for antenna in station.antennas.all().prefetch_related(
        "frequency_ranges", "antenna_type"
    ):
        for frequency_range in antenna.frequency_ranges.all():
            if (
                center_frequency
                and frequency_range.min_frequency
                <= center_frequency
                <= frequency_range.max_frequency
            ):
                is_center_frequency_in_station_range = True

    if center_frequency and not is_center_frequency_in_station_range:
        raise OutOfRangeError(
            "Center frequency({}) is not in station {} supported frequencies".format(
                format_frequency(center_frequency), station.id
            )
        )

    return Observation(
        satellite=sat,
        tle=tle_latest,
        author=author,
        start=start,
        end=end,
        ground_station=station,
        rise_azimuth=rise_azimuth,
        max_altitude=max_altitude,
        set_azimuth=set_azimuth,
        transmitter=transmitter,
        center_frequency=center_frequency or None,
        transmitter_frequency_low=transmitter.downlink_low or None,
        transmitter_frequency_high=transmitter.downlink_high or None,
        transmitter_frequency_drift=transmitter.downlink_drift or None,
        is_auto=is_auto,
    )


def get_available_stations(
    stations: QuerySet, downlink: int, user: User, satellite: Satellite
):
    """
    Returns stations for scheduling filtered by a specific downlink and user's permissions

    Args:
    stations(QuerySet): All available Stations
    downlink(int): Specific downlink for check availability
    user(User): User which permission for scheduling are checking
    satellite(Satellite): Satellite for checking in he is violator

    Returns:
        list: List of available stations for this User,Satellite and specific downlink
    """
    available_stations = []

    if satellite.is_frequency_violator:
        stations = stations.exclude(violator_scheduling=0)
        if not user.groups.filter(name="Operators").exists():
            stations = stations.exclude(violator_scheduling=1)

    stations_perms = schedule_stations_perms(user, stations)
    stations_with_permissions = [
        station for station in stations if stations_perms[station.id]
    ]
    for station in stations_with_permissions:
        # Skip if this station is not capable of receiving the frequency
        if not downlink:
            continue
        for gs_antenna in station.antennas.all():
            for frequency_range in gs_antenna.frequency_ranges.all():
                if (
                    frequency_range.min_frequency
                    <= downlink
                    <= frequency_range.max_frequency
                ):
                    available_stations.append(station)
                    break
            else:
                continue  # to the next antenna of the station
            break  # station added to the available stations

    return available_stations
