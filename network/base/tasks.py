"""SONIKS Network Celery task functions"""

import csv
import logging
import os
import random
import re
import struct
import tempfile
import zipfile
from datetime import datetime, timedelta, timezone
from time import sleep
from typing import List, Optional
from urllib.request import urlretrieve

import requests
from celery import group as celery_group
from celery import shared_task
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.files import File
from django.core.files.base import ContentFile
from django.core.mail import send_mail
from django.core.validators import URLValidator
from django.db import transaction
from django.db.models import Q
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from django.utils.timezone import make_aware, now, utc
from django.utils.translation import gettext_lazy as _
from internetarchive import upload
from internetarchive.exceptions import AuthenticationError
from mutagen.oggvorbis import OggVorbis
from tinytag import TinyTag
from tinytag.tinytag import TinyTagException

from network.base.auto_scheduling import autoschedule_predictions_for_satellite
from network.base.influx import decode_data
from network.base.launch_scheduler import (
    create_new_launch_observations,
    launch_scheduler,
)
from network.base.models import (
    DemodData,
    ExportedFrameset,
    LatestTleSet,
    Mode,
    NetworkSchedule,
    Observation,
    Operator,
    Satellite,
    Station,
    StationSchedule,
    StationScheduleSec,
    StationStat,
    Telemetry,
    Tle,
    Transmitter,
    TransmitterStats,
    generate_norad_id,
)
from network.base.orbital_satellite_params import OrbitalSatelliteParams
from network.base.rating_tasks import rate_observation
from network.base.signals import _station_post_save
from network.base.station_stat_calculator import StationStatCalculator
from network.base.validators import is_transmitter_in_station_range
from network.users.models import User

LOGGER = logging.getLogger("db")


def check_unknown_transmitters(launch=False):
    """
    Check if transmitter have  data for last month. If \\
    he doesnt- make him unknown \\
    """
    # pylint: disable=W0640
    # see https://github.com/pylint-dev/pylint/issues/7100
    filter_dict = {"status": "active"}
    if launch:
        filter_dict["satellite__launch__launch_date__range"] = (
            now() - timedelta(days=14),
            now(),
        )

    transmitters = Transmitter.objects.filter(**filter_dict)
    last_month = datetime.now(timezone.utc) - timedelta(days=30)
    all_obs = (
        Observation.objects.filter(
            end__lte=now(), status__in=[0, 100, -100]
        )  # Только хорошие, неизвестные и плохие
        .order_by("-end")
        .values(
            "status",
            "end",
            "transmitter",
        )
    )
    for transmitter in transmitters:
        trans_obs = list(filter(lambda x: x["transmitter"] == transmitter.id, all_obs))
        len_check = False
        date_check = False
        count_check = False
        if len(trans_obs) == 0:
            len_check = True
        else:
            if trans_obs[0]["end"] < last_month:
                date_check = True
            transmitter_all_obs_count = len(trans_obs)
            positive_obs = 0
            for one_obs in trans_obs:
                if one_obs["status"] == 100:
                    positive_obs += 1
            percentage_of_success = float(positive_obs / transmitter_all_obs_count)
            if percentage_of_success < 0.2:
                count_check = True
        if len_check or date_check or count_check:
            transmitters.filter(uuid=transmitter.uuid).update(unknown=True)
        else:
            transmitters.filter(uuid=transmitter.uuid).update(unknown=False)


def delay_task_with_lock(task, lock_id, lock_expiration, *args):
    """
    Ensure unique run of a task by aquiring lock
    """
    if cache.add("{0}-{1}".format(task.name, lock_id), "", lock_expiration):
        task.delay(*args)


def get_observation_zip_group(observation_id):
    """
    Return observation group
    """
    return (observation_id - 1) // settings.AUDIO_FILES_PER_ZIP


def get_zip_range_and_path(group):
    """
    Return range and zip filepath for a group of observation IDs
    """
    group *= settings.AUDIO_FILES_PER_ZIP
    group_range = (group + 1, group + settings.AUDIO_FILES_PER_ZIP)
    zip_range = "{0}-{1}".format(
        str(group_range[0]).zfill(9), str(group_range[1]).zfill(9)
    )
    zip_filename = "{0}-{1}.zip".format(settings.ZIP_FILE_PREFIX, zip_range)
    zip_path = "{0}/{1}".format(settings.MEDIA_ROOT, zip_filename)
    return (group_range, zip_path)


def update_satellites(satellites: List[dict]):
    """
    Updating satellites in accordance with SatNOGS

    Args:
        satellites(list[dict]): List of satellite dicts
    """
    satellites_added = 0
    satellites_updated = 0
    for satellite in satellites:
        if satellite["sat_id"]:
            norad = satellite["norad_cat_id"]
            if not norad:
                satellite["norad_cat_id"] = generate_norad_id()
            countries = satellite["countries"]
            satellite["countries"] = countries.split(",")
            operator = satellite["operator"]
            image_json = satellite["image"]
            satellite.pop("image", None)
            satellite.pop("operator", None)
            if not operator == "None":
                operator_obj, _ = Operator.objects.get_or_create(name=operator)
                satellite["operator_id"] = operator_obj.id
            telemetries = satellite["telemetries"]
            satellite.pop("telemetries", None)
            satellite.pop("updated", None)
            satellite.pop("citation", None)
            satellite.pop("associated_satellites", None)
            satellite["approved"] = True
            if satellite["website"]:
                satellite["website"] = satellite["website"].replace(
                    "geoscan.space/en", "geoscan.space/ru"
                )
            if not image_json == "":
                image = requests.get(f"https://db.satnogs.org/media/{image_json}")
            img_temp = tempfile.NamedTemporaryFile(delete=True)
            img_temp.write(image.content)
            img_temp.flush()
            try:
                # Update Satellite
                existing_satellite = Satellite.objects.get(sat_id=satellite["sat_id"])
                satellite.pop("is_frequency_violator", None)
                existing_satellite.__dict__.update(satellite)
                if not image_json == "" and not existing_satellite.image:
                    existing_satellite.image.save(
                        f"{existing_satellite.name}.jpeg", File(img_temp), save=True
                    )
                if len(telemetries) != 0:
                    Telemetry.objects.get_or_create(
                        satellite_id=existing_satellite.id,
                        name=telemetries[0]["decoder"],
                        decoder=telemetries[0]["decoder"],
                    )
                existing_satellite.save()
                satellites_updated += 1
            except Satellite.DoesNotExist:
                # Create Satellite
                sat_obj = Satellite.objects.create(**satellite)
                if len(telemetries) != 0:
                    Telemetry.objects.get_or_create(
                        satellite_id=sat_obj.id,
                        name=telemetries[0]["decoder"],
                        decoder=telemetries[0]["decoder"],
                    )
                if not image_json == "":
                    sat_obj.image.save(
                        f"{sat_obj.name}.jpeg", File(img_temp), save=True
                    )
                satellites_added += 1

    LOGGER.info(
        "Added/Updated %s/%s satellites from db.", satellites_added, satellites_updated
    )


def check_transmitter_freq(exist_transmitter: Transmitter, new_transmitter: dict):
    """
    Transmitter frequency change check function.
    If the frequency has changed, it changes the frequency of future observations only.

    Args:
        exist_transmitter (Transmitter): Existing transmitter with current data.
        new_transmitter (dict): New transmitter data received from Satnogs.
    """
    if (
        exist_transmitter.downlink_low != new_transmitter["downlink_low"]
        or exist_transmitter.downlink_high != new_transmitter["downlink_high"]
        or exist_transmitter.downlink_drift != new_transmitter["downlink_drift"]
    ):
        Observation.objects.filter(
            transmitter=exist_transmitter, start__gt=now()
        ).update(
            transmitter_frequency_low=new_transmitter["downlink_low"],
            transmitter_frequency_high=new_transmitter["downlink_high"],
            transmitter_frequency_drift=new_transmitter["downlink_drift"],
        )
        LOGGER.info(
            "Updated the observation frequency for the transmitter: %s",
            exist_transmitter.uuid,
        )


def update_transmitters(transmitters: List[dict]):
    """
    Updating transmitters in accordance with SatNOGS

    Args:
        transmitters(list[dict]): list of Transmitter dicts
    """
    transmitters_added = 0
    transmitters_updated = 0
    for transmitter in transmitters:
        if transmitter["uuid"]:
            satellite_id = transmitter["sat_id"]
            uplink_mode = transmitter["uplink_mode"]
            downlink_mode = transmitter["mode"]
            transmitter.pop("sat_id", None)
            transmitter.pop("alive", None)
            transmitter.pop("uplink_mode", None)
            transmitter.pop("mode", None)
            transmitter.pop("mode_id", None)
            transmitter.pop("norad_cat_id", None)
            transmitter.pop("norad_follow_id", None)
            transmitter.pop("updated", None)
            transmitter.pop("service", None)
            transmitter.pop("frequency_violation", None)
            transmitter.pop("transmitter_stats", None)
            transmitter["approved"] = True
            try:
                satellite_obj = Satellite.objects.get(sat_id=satellite_id)
                transmitter["satellite_id"] = satellite_obj.id
            except Satellite.DoesNotExist:
                transmitter["satellite_id"] = None

            if downlink_mode:
                downlink_mode_obj, _ = Mode.objects.get_or_create(name=downlink_mode)
                transmitter["downlink_mode_id"] = downlink_mode_obj.id
            else:
                transmitter["downlink_mode_id"] = None

            if uplink_mode:
                mode_obj, _ = Mode.objects.get_or_create(name=uplink_mode)
                transmitter["uplink_mode_id"] = mode_obj.id
            else:
                transmitter["uplink_mode_id"] = None

            try:
                # Update Transmitter
                existing_transmitter = Transmitter.objects.get(uuid=transmitter["uuid"])
                check_transmitter_freq(existing_transmitter, transmitter)
                existing_transmitter.create_stat_for_transmitter()
                existing_transmitter.__dict__.update(transmitter)
                existing_transmitter.save()
                transmitters_updated += 1
            except Transmitter.DoesNotExist:
                # Create Transmitter
                new_transmitter = Transmitter.objects.create(**transmitter)
                new_transmitter.create_stat_for_transmitter()
                transmitters_added += 1

    LOGGER.info(
        "Added/Updated %s/%s transmitters from db.",
        transmitters_added,
        transmitters_updated,
    )


def update_tle(tle_set: List[dict]):
    """
    Receive new tle for Satellites from SatNOGS and update satellites LatestTleSets

    Args:
        tle_set(list[dict]): List of satellites tle
    """
    tle_added = 0
    for tle in tle_set:
        try:
            sat_id = tle["sat_id"]
            tle.pop("sat_id", None)
            tle.pop("norad_cat_id", None)
            satellite = Satellite.objects.get(sat_id=sat_id)
            tle["satellite_id"] = satellite.id
            if Tle.objects.filter(
                satellite_id=satellite.id,
                tle0=tle["tle0"],
                tle1=tle["tle1"],
                tle2=tle["tle2"],
            ).exists():
                continue
            # if its new tle- add it. If it already exist-do nothink
            tle_new = Tle.objects.create(**tle)
            LatestTleSet.objects.update_or_create(
                satellite=satellite,
                defaults={"latest": tle_new, "last_modified": now()},
            )
            tle_added += 1
        except Satellite.DoesNotExist:
            pass
    LOGGER.info("Added and updated %s latest tle from db.", tle_added)


def notify_user_export(url: str, sat_id: str, email: str, subject: str, template: str):
    """
    Helper function to email a user when their export is complete

    Args:
        url(str): Url of frameset file
        sat_id(str): Sat id of satellite which data is exporting
        email(str): Email of user who send exporting request
        subject(str): subject for email
        template(str): path to template for email
    """
    url_validator = URLValidator()
    try:
        url_validator(url)
        data = {"url": url, "sat_id": sat_id}
    except ValidationError:
        site = Site.objects.get_current()
        data = {"url": "{0}{1}".format(site.domain, url), "sat_id": sat_id}
    message = render_to_string(template, {"data": data})
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email], False)


def create_demoddata_object(
    transmitter_uuid: str,
    timestamp: datetime,
    demodulated_data: bytes,
    data_filename: str,
    station_id: int,
):
    """
    Create demoddata object from SatNOGS frame

    Args:
        transmitter_uuid(str): UUID of transmitter which used in
            observation when file uploaded
        timestamp(datetime): Time when file was uploaded
        demodulated_data(bytes): Content of file in bytes
        data_filename(str): Name of file
        station_id(int): ID of station that receive file
    """
    try:
        satellite = Satellite.objects.get(transmitter_entries__uuid=transmitter_uuid)
        observer = "SatNOGS_" + str(station_id)
        try:
            data = DemodData.objects.get(
                satellite=satellite,
                station=None,
                timestamp=timestamp,
                sonik_data=False,
                demodulated_data__endswith=data_filename,
            )
        except DemodData.DoesNotExist:
            data = DemodData.objects.create(
                satellite=satellite,
                station=None,
                timestamp=timestamp,
                sonik_data=False,
                observer=observer,
            )
            data.demodulated_data.save(data_filename, ContentFile(demodulated_data))
        if not data.is_decoded:
            decode_current_frame.delay(sat_id=satellite.sat_id, demoddata_id=data.id)
    except Satellite.DoesNotExist:
        return


@shared_task
def zip_audio(observation_id, path):
    """Add audio file to a zip file"""
    LOGGER.info("zip audio: %s", observation_id)
    group = get_observation_zip_group(observation_id)
    group_range, zip_path = get_zip_range_and_path(group)
    cache_key = "{0}-{1}-{2}".format("ziplock", group_range[0], group_range[1])
    if cache.add(cache_key, "", settings.ZIP_AUDIO_LOCK_EXPIRATION):
        LOGGER.info("Lock acquired for zip audio: %s", observation_id)
        file_exists_in_zip_file = False
        files_in_zip = []
        if zipfile.is_zipfile(zip_path):
            with zipfile.ZipFile(file=zip_path, mode="r") as zip_file:
                files_in_zip = zip_file.namelist()
                filename = path.split("/")[-1]
                if filename in files_in_zip:
                    file_exists_in_zip_file = True
        if file_exists_in_zip_file:
            LOGGER.info(
                "Audio file already exists in zip file for id %s", observation_id
            )
            ids = [name.split("_")[1] for name in files_in_zip]
            observations = (
                Observation.objects.filter(pk__in=ids)
                .exclude(payload="")
                .exclude(archived=True)
            )
            if observations.count() == len(ids):
                observations.update(audio_zipped=False)
                os.remove(zip_path)
            else:
                cache.delete(cache_key)
                error_message = (
                    "Zip file can not be deleted,"
                    " it includes removed, archived or duplicate audio files"
                )
                raise RuntimeError(error_message)
        else:
            with zipfile.ZipFile(
                file=zip_path,
                mode="a",
                compression=zipfile.ZIP_DEFLATED,
                compresslevel=9,
            ) as zip_file:
                zip_file.write(filename=path, arcname=path.split("/")[-1])
            Observation.objects.filter(pk=observation_id).update(audio_zipped=True)
        cache.delete(cache_key)


@shared_task
def process_audio(observation_id: int, force_zip: bool = False):
    """
    Process Audio
    * Check audio file for duration less than 1 sec
    * Validate audio file
    * Run task for rating according to audio file
    * Run task for adding audio in zip file

    Args:
        observation_id(int): ID of observation which audio processing
        force_zip(bool): Zipping this audio or not. Default is False.
            Deactivated if audio stored in S3
    """
    LOGGER.info("process audio: %s", observation_id)
    observations = Observation.objects.select_for_update()
    with transaction.atomic():
        observation = observations.get(pk=observation_id)
        if settings.USE_S3_STORAGE_FOR_AUDIO:
            filename, _ = urlretrieve(observation.payload.url)
            audio_metadata_duration = OggVorbis(filename).info.length
        else:
            try:
                audio_metadata_duration = TinyTag.get(observation.payload.path).duration
            except (TinyTagException, (struct.error, TypeError)):
                observation.payload.delete()
                return
        # Remove audio if it is less than 1 sec
        if audio_metadata_duration is None or audio_metadata_duration < 1:
            observation.payload.delete()
            return
        rate_observation(observation_id, "audio_upload", audio_metadata_duration)
        if (
            settings.ZIP_AUDIO_FILES or force_zip
        ) and not settings.USE_S3_STORAGE_FOR_AUDIO:
            zip_audio(observation_id, observation.payload.path)
        return


@shared_task
def zip_audio_files(force_zip: bool = False):
    """
    Zip audio files per group
    """
    LOGGER.info("zip audio")
    if cache.add("zip-task", "", settings.ZIP_TASK_LOCK_EXPIRATION):
        LOGGER.info("Lock acquired for zip task")
        if (
            settings.ZIP_AUDIO_FILES or force_zip
        ) and not settings.USE_S3_STORAGE_FOR_AUDIO:
            zipped_files = []
            observations = Observation.objects.filter(audio_zipped=False).exclude(
                payload=""
            )
            non_zipped_ids = observations.order_by("pk").values_list("pk", flat=True)
            if non_zipped_ids:
                group = get_observation_zip_group(non_zipped_ids[0])
            for observation_id in non_zipped_ids:
                if group == get_observation_zip_group(observation_id):
                    process_audio(observation_id, force_zip)
                    zipped_files.append(observation_id)
                else:
                    LOGGER.info("Processed Files: %s", zipped_files)
                    cache.delete("zip-task")
                    return
            LOGGER.info("Processed Files: %s", zipped_files)
    cache.delete("zip-task")


def get_groups_for_archiving_audio_zip_files():
    """
    Returns the groups of audio files that haven't been archived yet
    """
    observation_ids = Observation.objects.filter(
        audio_zipped=True, archived=False
    ).values_list("pk", flat=True)
    return {get_observation_zip_group(pk) for pk in observation_ids}


@shared_task
def archive_audio_zip_files(force_archive: bool = False):  # pylint: disable=R0915
    """
    Archive audio zip files to archive.org
    """
    LOGGER.info("archive audio")
    if cache.add("archive-task", "", settings.ARCHIVE_TASK_LOCK_EXPIRATION):
        LOGGER.info("Lock acquired for archive task")
        if (
            settings.ARCHIVE_ZIP_FILES or force_archive
        ) and not settings.USE_S3_STORAGE_FOR_AUDIO:
            archived_groups = []
            skipped_groups = []
            archive_skip_time = now() - timedelta(hours=settings.ARCHIVE_SKIP_TIME)
            groups = get_groups_for_archiving_audio_zip_files()
            for group in groups:
                group_range, zip_path = get_zip_range_and_path(group)
                cache_key = "{0}-{1}-{2}".format(
                    "ziplock", group_range[0], group_range[1]
                )
                if (
                    (not cache.add(cache_key, "", settings.ARCHIVE_ZIP_LOCK_EXPIRATION))
                    or (not zipfile.is_zipfile(zip_path))
                    or Observation.objects.filter(
                        Q(end__gte=archive_skip_time)
                        | Q(archived=True)
                        | (Q(audio_zipped=False) & (~Q(payload="")))
                    )
                    .filter(pk__range=group_range)
                    .exists()
                ):
                    skipped_groups.append(group_range)
                    cache.delete(cache_key)
                    continue

                archived_groups.append(group_range)
                site = Site.objects.get_current()
                license_url = "http://creativecommons.org/licenses/by-sa/4.0/"

                item_group = group // settings.ZIP_FILES_PER_ITEM
                files_per_item = (
                    settings.ZIP_FILES_PER_ITEM * settings.AUDIO_FILES_PER_ZIP
                )
                item_from = (item_group * files_per_item) + 1
                item_to = (item_group + 1) * files_per_item
                item_range = "{0}-{1}".format(
                    str(item_from).zfill(9), str(item_to).zfill(9)
                )

                item_id = "{0}-{1}".format(settings.ITEM_IDENTIFIER_PREFIX, item_range)
                title = "{0} {1}".format(settings.ITEM_TITLE_PREFIX, item_range)
                description = (
                    '<p>Audio files from <a href="{0}/observations">'
                    "SONIKS Observations</a> with ID from {1} to {2}.</p>"
                ).format(site.domain, item_from, item_to)

                item_metadata = dict(
                    collection=settings.ARCHIVE_COLLECTION,
                    title=title,
                    mediatype=settings.ARCHIVE_MEDIA_TYPE,
                    licenseurl=license_url,
                    description=description,
                )

                zip_name = zip_path.split("/")[-1]
                file_metadata = dict(
                    name=zip_path,
                    title=zip_name.replace(".zip", ""),
                    license_url=license_url,
                )

                try:
                    res = upload(
                        item_id,
                        files=file_metadata,
                        metadata=item_metadata,
                        access_key=settings.S3_ACCESS_KEY,
                        secret_key=settings.S3_SECRET_KEY,
                        retries=settings.S3_RETRIES_ON_SLOW_DOWN,
                        retries_sleep=settings.S3_RETRIES_SLEEP,
                    )
                except (
                    requests.exceptions.RequestException,
                    AuthenticationError,
                ) as error:
                    LOGGER.info(
                        "Upload of zip %s failed, reason:\n%s", zip_name, repr(error)
                    )
                    continue

                if res[0].status_code == 200:
                    observations = (
                        Observation.objects.select_for_update()
                        .filter(pk__range=group_range)
                        .filter(audio_zipped=True)
                    )
                    with transaction.atomic():
                        for observation in observations:
                            audio_filename = observation.payload.name.split("/")[-1]
                            observation.archived = True
                            observation.archive_url = "{0}{1}/{2}/{3}".format(
                                settings.ARCHIVE_URL, item_id, zip_name, audio_filename
                            )
                            observation.archive_identifier = item_id
                            if settings.REMOVE_ARCHIVED_AUDIO_FILES:
                                if observation.payload:
                                    observation.payload.delete(save=False)
                            observation.save(
                                update_fields=[
                                    "archived",
                                    "archive_url",
                                    "archive_identifier",
                                    "payload",
                                ]
                            )
                    if settings.REMOVE_ARCHIVED_ZIP_FILE:
                        os.remove(zip_path)
                cache.delete(cache_key)
            cache.delete("archive-task")
            LOGGER.info("Archived Groups: %s", archived_groups)
            LOGGER.info("Skipped Groups: %s", skipped_groups)


@shared_task
def update_future_observations_with_new_tle_sets():
    """
    Update future observations with latest TLE sets
    """
    start = now() + timedelta(minutes=10)
    future_observations = Observation.objects.filter(start__gt=start)
    sat_id_set = set(future_observations.values_list("satellite__sat_id", flat=True))
    for sat_id in sat_id_set:
        try:
            future_observations.filter(satellite__sat_id=sat_id).update(
                tle=LatestTleSet.objects.get(satellite__sat_id=sat_id).latest
            )
            LOGGER.info("Success update TLE for observation with sat_id = %s", sat_id)
        except LatestTleSet.DoesNotExist:
            LOGGER.info(
                "for some reason satellite %s: dont have LatestTleSet Object", sat_id
            )


@shared_task
def fetch_data():
    """
    Fetch data from Satnogs DB

    Raises:
        requests.exceptions.ConnectionError
    """

    db_api_url = settings.DB_API_ENDPOINT
    if not db_api_url:
        LOGGER.info("Zero length api url, fetching is stopped")
        return

    satellites_url = "{}satellites".format(db_api_url)
    LOGGER.info("Fetching Satellites from %s", satellites_url)
    r_satellites = requests.get(satellites_url)
    update_satellites(r_satellites.json())

    transmitters_url = "{}transmitters".format(db_api_url)
    LOGGER.info("Fetching Transmitters from %s", transmitters_url)
    r_transmitters = requests.get(transmitters_url)
    update_transmitters(r_transmitters.json())

    tle_url = "{}tle".format(db_api_url)
    LOGGER.info("Fetching Latest tle from %s", tle_url)
    r_tle = requests.get(tle_url)
    update_tle(r_tle.json())


@shared_task
def clean_observations():
    """
    Task to clean up old observations that lack actual data
    *Не используется

    """
    threshold = now() - timedelta(days=int(settings.OBSERVATION_OLD_RANGE))
    observations = Observation.objects.filter(
        end__lt=threshold, archived=False
    ).exclude(payload="")
    for obs in observations:
        if settings.ENVIRONMENT == "test":
            if not obs.status >= 100:
                obs.delete()
                continue


@shared_task
def station_status_update():
    """
    Task to update Station status
    """
    post_save.disconnect(_station_post_save, sender=Station)
    for station in Station.objects.all():
        station.update_status(created=False)
    post_save.connect(_station_post_save, sender=Station)


@shared_task
def notify_for_stations_without_results():
    """
    Task to send email for stations with observations without results
    """
    email_to = settings.EMAIL_FOR_STATIONS_ISSUES
    if email_to:
        stations = ""
        obs_limit = settings.OBS_NO_RESULTS_MIN_COUNT
        time_limit = now() - timedelta(seconds=settings.OBS_NO_RESULTS_IGNORE_TIME)
        last_check = time_limit - timedelta(
            seconds=settings.OBS_NO_RESULTS_CHECK_PERIOD
        )
        for station in Station.objects.filter(status=2):
            last_obs = Observation.objects.filter(
                ground_station=station, end__lt=time_limit
            ).order_by("-end")[:obs_limit]
            obs_without_results = 0
            obs_after_last_check = False
            for observation in last_obs:
                if not (observation.has_audio and observation.has_waterfall):
                    obs_without_results += 1
                if observation.end >= last_check:
                    obs_after_last_check = True
            if obs_without_results == obs_limit and obs_after_last_check:
                stations += " " + str(station.id)
        if stations:
            # Notify user
            subject = _("[soniks.space] Станция с наблюдениями без результатов")
            send_mail(
                subject,
                stations,
                settings.DEFAULT_FROM_EMAIL,
                [settings.EMAIL_FOR_STATIONS_ISSUES],
                False,
            )


@shared_task
def update_transmitters_stats():
    """
    Periodic task to update TransmitterStats for all transmitters
    If TransmitterStats does not exist, it will be created
    """

    transmitters = Transmitter.objects.all()
    for transmitter in transmitters:
        observations = Observation.objects.filter(transmitter=transmitter)
        transmitter_stats, _ = TransmitterStats.objects.get_or_create(
            transmitter=transmitter
        )
        transmitter_stats.total_count = observations.count()
        transmitter_stats.unknown_count = observations.filter(
            status__gte=0, status__lt=100
        ).count()
        transmitter_stats.good_count = observations.filter(status__gte=100).count()
        transmitter_stats.bad_count = observations.filter(
            status__gte=-100, status__lt=0
        ).count()
        transmitter_stats.failed_count = observations.filter(status__lt=-100).count()
        transmitter_stats.save()
    LOGGER.info("TransmitterStats updated!")


@shared_task
def decode_current_frame(sat_id: str, demoddata_id: int):
    """
    Task to trigger a decode of a current frame for a satellite.\\
    Trying to decode even if file was already decoded

    Args:
        sat_id(str): Sat id of satellite which data is decoding
        demoddata_id(int): ID of demoddata which decoding
    """
    sleep(2)  # Give some time for database and s3 to be updated
    LOGGER.info("Decode demoddata %s for satellite", sat_id)
    decode_data(sat_id=sat_id, demoddata_id=demoddata_id, redecode=True)


@shared_task
def decode_recent_data():
    """
    Task to trigger a decode of data for all satellites no older than a week
    """
    satellites = Satellite.objects.all()
    for satellite in satellites:
        LOGGER.info(
            "Decode data for satellite %s no older than a week", satellite.sat_id
        )
        try:
            decode_data(sat_id=satellite.sat_id, decode_last_week=True)
        except:
            LOGGER.info(
                "Error while decoding data for satellite %s no older than a week",
                satellite.sat_id,
            )


@shared_task
def decode_all_data(sat_id: str):
    """
    Task to trigger a full decode of data for a satellite in admin panel

    Args:
        sat_id(str): Sat id of satellite which data will be full decoded or redecoded
    """
    decode_data(sat_id=sat_id, redecode=True)


@shared_task
def decode_all_nondecoded_data(sat_id: str):
    """
    Task to trigger a decode of non decoded data for a satellite in admin panel

    Args:
        sat_id(str): Sat id of satellite which data will be decoded
    """
    decode_data(sat_id=sat_id)


@shared_task
def remove_old_exported_framesets():
    """
    Task to remove exported framesets older then settings parameter

    TODO: Убрать utcnow(), переделать под now(timezone.utc)
    """
    old_datetime = make_aware(
        datetime.utcnow() - timedelta(seconds=settings.EXPORTED_FRAMESET_TIME_TO_LIVE)
    )
    exported_framesets = ExportedFrameset.objects.filter(
        created__lte=old_datetime
    ).exclude(exported_file="")
    for frameset in exported_framesets:
        frameset.exported_file.delete()


@shared_task
def export_frames(sat_id: str, user_pk: int, period: Optional[int] = None):
    """
    Task to export satellite frames in csv

    Args:
        sat_id(str): Sat id of satellite which data is exporting
        user_pk(int): ID of user who request exporting
        period(int,optional): number represent a period of exporting
            1-week,2(or other num)-month,None-all

    TODO: Убрать utcnow(), переделать под now(timezone.utc)
    """
    exported_frameset = ExportedFrameset()
    satellite = Satellite.objects.get(sat_id=sat_id)
    exported_frameset.user = get_user_model().objects.get(pk=user_pk)
    exported_frameset.satellite = satellite
    exported_frameset.end = datetime.utcnow()
    if period is not None:
        if period == 1:
            exported_frameset.start = make_aware(
                exported_frameset.end - timedelta(days=7)
            )
            suffix = "week"
        else:
            exported_frameset.start = make_aware(
                exported_frameset.end - timedelta(days=30)
            )
            suffix = "month"
        frames = DemodData.objects.filter(
            satellite=satellite,
            timestamp__gte=exported_frameset.start,
            timestamp__lte=exported_frameset.end,
            is_image=False,
        )
    else:
        frames = DemodData.objects.filter(
            satellite=satellite,
            timestamp__lte=exported_frameset.end,
            is_image=False,
        )
        suffix = "all"
    filename = "{0}-{1}-{2}-{3}.csv".format(
        sat_id, user_pk, exported_frameset.end.strftime("%Y%m%dT%H%M%SZ"), suffix
    )
    with tempfile.SpooledTemporaryFile(max_size=16777216, mode="w+") as csv_file:
        writer = csv.writer(csv_file, delimiter="|")
        for obj in frames:
            try:
                with obj.demodulated_data.storage.open(
                    obj.demodulated_data.name, "r"
                ) as file:
                    data = file.read()
                    if re.fullmatch(r"^[A-F0-9]+$", data):
                        frame = data
                    else:
                        frame = obj.display_payload_hex().replace(" ", "")
            except (TypeError, UnicodeDecodeError):
                frame = obj.display_payload_hex().replace(" ", "")

            if frame is not None:
                writer.writerow(
                    [
                        obj.timestamp.strftime("%Y-%m-%d %H:%M:%S"),
                        frame,
                    ]
                )
        content_file = File(csv_file)
        exported_frameset.exported_file.save(filename, content_file)
    exported_frameset.end = datetime.now(
        timezone.utc
    )  # if writer works too much user have less time to download files,so we should get timestamp after export
    subject = _("[soniks.space] Ваш запрос на загрузку данных готов!")
    template = "account/email/exported_frameset.txt"
    notify_user_export(
        url=exported_frameset.exported_file.url,
        sat_id=sat_id,
        email=exported_frameset.user.email,
        subject=subject,
        template=template,
    )


@shared_task
def export_tle(sat_id: str, user_pk: int, period: Optional[int] = None):
    """
    Task to export satellite TLE in csv

    Args:
        sat_id(str): Sat id of satellite which data is exporting
        user_pk(int): ID of user who request exporting
        period(int,optional): number represent a period of exporting
            1-week,2(or other num)-month,None-all
    """
    exported_frameset = ExportedFrameset()
    satellite = Satellite.objects.get(sat_id=sat_id)
    exported_frameset.user = get_user_model().objects.get(pk=user_pk)
    exported_frameset.satellite = satellite
    exported_frameset.end = datetime.now(timezone.utc)
    if period is not None:
        if period == 1:
            exported_frameset.start = exported_frameset.end - timedelta(days=7)
            suffix = "week"
        else:
            exported_frameset.start = exported_frameset.end - timedelta(days=30)
            suffix = "month"
        tles = Tle.objects.filter(
            satellite=satellite,
            updated__gte=exported_frameset.start,
            updated__lte=exported_frameset.end,
        ).order_by("updated")
    else:
        tles = Tle.objects.filter(
            satellite=satellite,
            updated__lte=exported_frameset.end,
        ).order_by("updated")
        suffix = "all"
    filename = "{0}-{1}-{2}-{3}_tle.csv".format(
        sat_id, user_pk, exported_frameset.end.strftime("%Y%m%dT%H%M%SZ"), suffix
    )
    with tempfile.SpooledTemporaryFile(max_size=16777216, mode="w+") as csv_file:
        writer = csv.writer(csv_file, delimiter="|")
        for tle in tles:
            writer.writerow([tle.tle0, tle.tle1, tle.tle2, tle.updated])
        content_file = File(csv_file)
        exported_frameset.exported_file.save(filename, content_file)
    exported_frameset.end = datetime.now(timezone.utc)
    subject = _("[soniks.space] Ваш запрос на загрузку TLE готов!")
    template = "account/email/exported_tle.txt"
    notify_user_export(
        url=exported_frameset.exported_file.url,
        sat_id=sat_id,
        email=exported_frameset.user.email,
        subject=subject,
        template=template,
    )


@shared_task
def send_current_data_to_satnogs(data_id: int):
    """
    Send demodData to SatNOGS by SIDS

    Args:
        data_id(int): ID of demodData which sending to SatNOGS
    """
    if settings.DEBUG:
        return

    url = "{0}telemetry/".format(settings.DB_API_ENDPOINT)
    try:
        data = DemodData.objects.get(id=data_id)
        norad = data.satellite.norad_cat_id
        source = "SONIKS: Station_" + str(data.station.id)
        locator = "longLat"
        latitude = data.lat
        longitude = data.lng
        timestamp = data.timestamp.strftime("%Y-%m-%dT%H:%M:%SZ")
        frame = data.display_payload_hex().replace(" ", "")

        payload = {
            "noradID": norad,
            "source": source,
            "locator": locator,
            "latitude": latitude,
            "longitude": longitude,
            "timestamp": timestamp,
            "frame": frame,
        }

        response = requests.post(url, data=payload)
        if response.status_code == 201:
            data.sent_to_satnogs = True
            data.save()
        LOGGER.info(
            "Send data to Satnogs, data_id: %d, status: %d",
            data.id,
            response.status_code,
        )

    except DemodData.DoesNotExist:
        LOGGER.info("DemodData with id %d does not exist", data_id)
    except requests.exceptions.RequestException as e:
        LOGGER.error(
            "Failed to send data to Satnogs, data_id: %d, error: %s", data_id, str(e)
        )
    except TimeoutError as e:
        LOGGER.error(
            "Failed to send data to Satnogs, data_id: %d, error: %s", data_id, str(e)
        )


@shared_task
def parse_data_from_satnogs(
    start: Optional[datetime] = None,
    end: Optional[datetime] = None,
    norad: Optional[str] = None,
):
    """
    Parse data from satnogs observations

    Args:
        start(datetime,optional): Start time of parsing pediod.If None then
            parse from last day
        end(datetime,optional): End time of parsing period. If None them
            parse to now
        norad(str): Parse satellite with target norad. If None parse all
            satellites

    """
    # off this task in develop mode
    if settings.DEBUG:
        return

    LOGGER.info("Start parse_data_from_satnogs")
    norad_id = ""
    if norad:
        norad_id = str(norad)

    if end is None:
        end = now().strftime("%Y-%m-%dT%H:%M")

    if start is None:
        start = (now() - timedelta(hours=25)).strftime("%Y-%m-%dT%H:%M")

    obs_url = "{0}observations/?format=json&start={1}&end={2}&satellite__norad_cat_id={3}".format(
        settings.NETWORK_API_ENDPOINT, start, end, norad_id
    )
    response = requests.get(obs_url)
    while response.status_code == 200:
        obs_all = response.json()
        for obs in obs_all:
            links = obs["demoddata"]
            for item in links:
                link = item["payload_demod"]
                if not link.endswith(".png") and not link.endswith(".jpg"):
                    file_name = link.split("/")[-1]
                    data = requests.get(link).content
                    timestamp = make_aware(
                        datetime.strptime(
                            file_name.split("_")[2].split(".")[0],
                            "%Y-%m-%dT%H-%M-%S",
                        ),
                        utc,
                    )
                    create_demoddata_object(
                        obs["transmitter"],
                        timestamp,
                        data,
                        file_name,
                        obs["ground_station"],
                    )
        try:
            obs_url = response.links["next"]["url"]
        except KeyError:
            break

        response = requests.get(obs_url)


@shared_task
def check_unknown_satellites(launch=False):
    """
    Check if satellite have active transmitters.If \\
    all his transmitters is inactive - make satellite \\
    unknown
    """
    # pylint: disable=W0640
    # see https://github.com/pylint-dev/pylint/issues/7100
    check_unknown_transmitters(launch)
    satellite_filter = {"status": "alive"}
    transmitter_filter = {"status": "active"}
    if launch:
        satellite_filter["launch__launch_date__range"] = (
            now() - timedelta(days=14),
            now(),
        )
        transmitter_filter["satellite__launch__launch_date__range"] = (
            now() - timedelta(days=14),
            now(),
        )
    satellites = Satellite.objects.filter(**satellite_filter)
    transmitters = Transmitter.objects.filter(**transmitter_filter).values(
        "unknown", "satellite_id"
    )
    for satellite in satellites:
        sat_transmitters = list(
            filter(lambda x: x["satellite_id"] == satellite.id, transmitters)
        )
        check = True
        for transmitter in sat_transmitters:
            if transmitter["unknown"] is False:
                check = False
        Satellite.objects.filter(sat_id=satellite.sat_id).update(unknown=check)


@shared_task
def auto_schedule_network(hours_for_predict: int):
    """
    Autoscheduling SONIKS network in three stages:
        - Stage one: schedule primary schedules on stations
        - Stage two: schedule network schedule on stations
        - Stage three: schedule seconddary schedules of stations

    Args:
        hours_for_predict (int): How many hours ahead observations are planned
    """
    station_schedules = StationSchedule.objects.filter(
        active=True, station__status=2
    ).select_related("station")
    # Stage 1 - Schedule primary schedule of station
    LOGGER.info("Stage 1")
    LOGGER.info("Schedule primary schedule on stations")
    first_stage_tasks = []
    if station_schedules:
        for primary_station_schedule in station_schedules:
            LOGGER.info("Schedule station %s", primary_station_schedule.station.id)
            first_stage_tasks.append(
                auto_schedule_station_primary.s(
                    primary_station_schedule.id, hours_for_predict
                )
            )
        first_stage_tasks_group = celery_group(first_stage_tasks)
        first_stage_result = first_stage_tasks_group.apply_async()
        while not first_stage_result.ready():
            sleep(1)
        LOGGER.info("All tasks of stage 1 is completed")
    else:
        LOGGER.info(
            "All station schedules inactive,all station with schedule \n \
            is offline or no station schedules have been created"
        )
        LOGGER.info("Skip stage 1")
    LOGGER.info("Stage 1 end")
    # Stage 2 - Schedule primary schedule of network
    LOGGER.info("Stage 2 start")
    second_stage_tasks = []
    try:
        for station in Station.objects.filter(status=2):
            second_stage_tasks.append(
                auto_schedule_station_network.s(station.id, hours_for_predict)
            )
        if len(second_stage_tasks) != 0:
            second_stage_tasks_group = celery_group(second_stage_tasks)
            second_stage_result = second_stage_tasks_group.apply_async()
            while not second_stage_result.ready():
                sleep(1)
            LOGGER.info("All tasks of stage 2 is completed")
        else:
            LOGGER.info("All stations is offline or testing. Skip stage 2")
    except NetworkSchedule.DoesNotExist:
        LOGGER.info("Network schedule not created,skip stage 2")
    LOGGER.info("Stage 2 end")
    # Stage 3 - Schedule second schedule of station
    LOGGER.info("Stage 3 start")
    third_stage_tasks = []
    station_schedules_sec = StationScheduleSec.objects.filter(
        active=True, station__status=2
    ).select_related("station")
    if station_schedules_sec:
        for second_station_schedule in station_schedules_sec:
            LOGGER.info(
                "Schedule station %s without priority",
                second_station_schedule.station.id,
            )
            third_stage_tasks.append(
                auto_schedule_station_second.s(
                    second_station_schedule.id, hours_for_predict
                )
            )
        third_stage_tasks_group = celery_group(third_stage_tasks)
        third_stage_result = third_stage_tasks_group.apply_async()
        while not third_stage_result.ready():
            sleep(1)
        LOGGER.info("All tasks of stage 3 is completed")
    else:
        LOGGER.info(
            "All station second schedules inactive,all station with schedule is \n \
            offline or no station second schedules have been created"
        )
        LOGGER.info("Skip stage 3")
    LOGGER.info("All stages ended")
    LOGGER.info("Auto scheduling completed")


@shared_task
def auto_schedule_station_primary(schedule_id: int, hours_for_schedule: int):
    """
    Schedule one station with primary schedule(with priority,stage 1)

    Args:
        schedule_id (int): ID of primary schedule
        hours_for_schedule (int): Hours for scheduling
    """
    station_schedule = StationSchedule.objects.select_related("station").get(
        id=schedule_id
    )
    # sort by priority, which in case of competition
    # of higher priority satellites planned
    satellites_sorted = sorted(
        station_schedule.satellites, key=lambda satellite: int(satellite["prio"])
    )
    for satellite in satellites_sorted:
        LOGGER.info(
            "Schedule satellite:%s for station %s",
            satellite["sat_name"],
            station_schedule.station.id,
        )
        start = datetime.now(timezone.utc)
        end = datetime.now(timezone.utc) + timedelta(hours=hours_for_schedule)
        LOGGER.info(
            "Satellite:%s start time:%s,end time:%s", satellite["sat_name"], start, end
        )
        try:
            obs_count = autoschedule_predictions_for_satellite(
                station_id=station_schedule.station.id,
                sat_id=satellite["sat_id"],
                transmitter_uuid=satellite["transmitter_uuid"],
                start_time=start,
                end_time=end,
                params=station_schedule.param,
            )
            LOGGER.info(
                "Scheduled: %s for station %s and satellite %s",
                obs_count,
                station_schedule.station.id,
                satellite["sat_name"],
            )
        except Exception:  # pylint: disable=W0703
            LOGGER.info(
                "Error occured while scheduling:%s on station %s",
                satellite["sat_name"],
                station_schedule.station.id,
            )


@shared_task
def auto_schedule_station_second(schedule_id: int, hours_for_schedule: int):
    """
    Scheduling one station with second schedule(without priority,stage 3)

    Args:
        schedule_id (int): ID of second schedule
        hours_for_schedule (int): Hours for scheduling
    """
    station_schedule = StationScheduleSec.objects.select_related("station").get(
        id=schedule_id
    )
    if station_schedule.station.status == 0:
        LOGGER.info("Station %s is offline", station_schedule.station.name)
        return
    satellite_shuffled = station_schedule.satellites
    random.shuffle(satellite_shuffled)
    for satellite in satellite_shuffled:
        LOGGER.info(
            "Schedule satellite:%s for station %s without priority",
            satellite["sat_name"],
            station_schedule.station.id,
        )
        start = datetime.now(timezone.utc)
        end = datetime.now(timezone.utc) + timedelta(hours=hours_for_schedule)
        LOGGER.info(
            "Satellite:%s start time:%s,end time:%s", satellite["sat_name"], start, end
        )
        try:
            obs_count = autoschedule_predictions_for_satellite(
                station_id=station_schedule.station.id,
                sat_id=satellite["sat_id"],
                transmitter_uuid=satellite["transmitter_uuid"],
                start_time=start,
                end_time=end,
                params=station_schedule.param,
            )
            LOGGER.info(
                "Scheduled:%s for station %s and satellite %s without priority",
                obs_count,
                station_schedule.station.client_id,
                satellite["sat_name"],
            )
        except Exception:  # pylint: disable=W0703
            LOGGER.info(
                "Error occured while scheduling:%s on station %s without priority",
                satellite["sat_name"],
                station_schedule.station.id,
            )


@shared_task
def auto_schedule_station_network(station_id: int, hours_for_schedule: int):
    """
    Schedule one station with network schedule(stage 2)

    Args:
        station_id (int): ID of station for scheduling
        hours_for_schedule (int): Hours for schedule
    """
    try:
        network_schedule = NetworkSchedule.objects.get(id=1)
    except NetworkSchedule.DoesNotExist:
        LOGGER.info("Network schedule not found")
        return
    station = Station.objects.get(id=station_id)
    satellites_sorted = sorted(
        network_schedule.satellites, key=lambda satellite: int(satellite["prio"])
    )
    network_user = User.objects.get(username="admin")
    for satellite in satellites_sorted:
        LOGGER.info(
            "Schedule satellite:%s for station %s in network scheduling",
            satellite["sat_name"],
            station.id,
        )
        start = datetime.now(timezone.utc)
        end = datetime.now(timezone.utc) + timedelta(hours=hours_for_schedule)
        LOGGER.info(
            "Satellite:%s start time:%s,end time:%s", satellite["sat_name"], start, end
        )
        try:
            transmitters_all = Transmitter.objects.filter(
                approved=True,
                satellite__sat_id=satellite["sat_id"],
                status__in=["active", "inactive"],
            )
            transmitter_find = False
            for transmitter in transmitters_all:
                if is_transmitter_in_station_range(transmitter, station):
                    transmitter_uuid = transmitter.uuid
                    transmitter_find = True
                    break
            if transmitter_find:
                obs_count = autoschedule_predictions_for_satellite(
                    station_id=station.id,
                    sat_id=satellite["sat_id"],
                    transmitter_uuid=transmitter_uuid,
                    start_time=start,
                    end_time=end,
                    params=network_schedule.param,
                    scheduled_user_name=network_user,
                )
                LOGGER.info(
                    "Scheduled: %s for station %s and satellite %s while network scheduling",
                    obs_count,
                    station.id,
                    satellite["sat_name"],
                )
            else:
                LOGGER.info(
                    "Cannot schedule %s for station %s - this satellite doesnt \n \
                    have transmitter in station frequency range",
                    satellite["sat_name"],
                    station.id,
                )
        except Exception:  # pylint: disable=W0703
            LOGGER.info(
                "Error occured while scheduling:%s on station %s while network scheduling",
                satellite["sat_name"],
                station.id,
            )


@shared_task
def calculate_all_satellite_orbital():
    """
    Calculate orbital params for all satellite in network.\\
    Calculate altutude,apogee,perigee and decay date
    """
    LOGGER.info("Start calculate_satellite_orbital")
    satellites = (
        Satellite.objects.filter(
            status="alive",
            latest_tle_set__last_modified__gt=(now() - timedelta(days=14)),
        )
        .only("sat_id", "name", "latest_tle_set")
        .prefetch_related("latest_tle_set__latest")
    )
    for satellite in satellites:
        LOGGER.info(
            "Calculating orbital params with sat_id %s is started", satellite.sat_id
        )
        orbital = OrbitalSatelliteParams(
            satellite.name,
            satellite.latest_tle_set.latest.tle1,
            satellite.latest_tle_set.latest.tle2,
        )
        satellite.altitude = orbital.get_altitude()
        satellite.apogee = orbital.get_apogee()
        satellite.perigee = orbital.get_perigee()
        decay_date = orbital.get_decay_date()
        satellite.decay_date = decay_date
        satellite.save()
        LOGGER.info(
            "Calculating orbital params with sat_id %s is ended", satellite.sat_id
        )
    LOGGER.info("End calculate_satellite_orbital")


@shared_task
def launch_scheduler_task(
    launch_id: int,
    start: datetime,
    end: datetime,
    violator_count: int,
    user_id: int,
    horizon: int,
    elevation: int,
    duration: int,
    sat_status: str,
):
    """
    Starting calculation and planning of observations of launch satellites

    Args:
        launch_id (int): ID of launch
        start (datetime): Start of planning period
        end (datetime): End of planning period
        violator_count (int): Count of observations for violator satellites
        user_id (int): ID of user - author of observations
        horizon (int): Minimum station horizon
        elevation (int): Minimum satellite elevation
        duration (int): Duration of observation
        sat_status (str): Satellite status for planning (None, alive or unknown)
    """
    user = User.objects.get(id=user_id)
    launch_observations = launch_scheduler(
        launch_id, start, end, violator_count, horizon, elevation, duration, sat_status
    )
    LOGGER.info(
        "Available windows for observations found: %d", len(launch_observations)
    )
    scheduled_observations = create_new_launch_observations(launch_observations, user)
    LOGGER.info(
        "Scheduled observations: %d", scheduled_observations["Total_Observations"]
    )
    subject = _("[soniks.space] Планирование спутников запуска завершено!")
    send_mail(
        subject,
        str(scheduled_observations),
        settings.DEFAULT_FROM_EMAIL,
        [settings.EMAIL_ADMIN],
        False,
    )


@shared_task
def calculate_all_station_stat():
    """
    Task to calculate statistics for all stations
    """
    stations = Station.objects.all().order_by("id")
    for station in stations:
        calculate_station_stat.delay(station.id)


@shared_task
def calculate_station_stat(station_id: int = 1):
    # calculate stat
    LOGGER.info("Start calculate_statistics for station %s", station_id)
    station_stat_calculator = StationStatCalculator(station_id=station_id)
    station_stat_calculator.set_demoddata_coordinates()
    LOGGER.info("Demoddata coordinates calculated for station %s", station_id)
    alt_svg = station_stat_calculator.calculate_svg_altitude()
    LOGGER.info("Altitude svg calculated for station %s", station_id)
    az_svg = station_stat_calculator.calculate_svg_azimuth()
    LOGGER.info("Azimuth svg calculated for station %s", station_id)
    low_az_svg = station_stat_calculator.calculate_svg_azimuth(elevation_max=31)
    LOGGER.info(
        "Azimuth svg with angle less than 31 calculated for station %s", station_id
    )
    polar_svg = station_stat_calculator.calculate_svg_polar(time_period="month")
    LOGGER.info("Polar svg calculated for station %s", station_id)

    # save stat
    station_stat_model, _ = StationStat.objects.get_or_create(
        station=station_stat_calculator.station
    )
    station_stat_model.all_alt_svg.save(
        "station_{}_all_alt.svg".format(station_id),
        ContentFile(alt_svg.encode("utf-8")),
    )
    station_stat_model.all_az_svg.save(
        "station_{}_all_az.svg".format(station_id), ContentFile(az_svg.encode("utf-8"))
    )
    station_stat_model.low_az_svg.save(
        "station_{}_low_az.svg".format(station_id),
        ContentFile(low_az_svg.encode("utf-8")),
    )
    station_stat_model.month_polar_svg.save(
        "station_{}_polar.svg".format(station_id),
        ContentFile(polar_svg.encode("utf-8")),
    )
    LOGGER.info("End calculate_statistics for station %s", station_id)
