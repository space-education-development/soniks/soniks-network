from django.db import migrations


def forwards(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    Group.objects.create(name='Operators')


def backwards(apps, schema_editor):
    """Delete Peers group."""
    Group = apps.get_model('auth', 'Group')
    Group.objects.filter(name='Operators').delete()


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_create_moderators_group'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
