/* global Slider,gettext  */


$(document).ready(function () {
    $('.selectpicker').selectpicker();
    
    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width());
        });
        return $helper;
    };

    $('#satellite-table_sec tbody').sortable({
        helper: fixHelperModified,
        stop:function(){
            save_schedule();
        }
    }).disableSelection();
	
    $('#satellite-table_sec tbody').sortable({
        distance: 5,
        delay: 100,
        opacity: 0.6,
        cursor: 'move',
    });

    function get_satellites(){
        var station_id=$('#station-info').data('id');
        var url=`/api/satellites/?format=json&status=alive&is_frequency_violator=false&station_id=${station_id}`;
        $.ajax({
            url: url,
            dataType: 'json',
        }).done(function(data){
            var select = document.getElementById('satellite-selection_sec');
            for (let i=0;i< data.length;i++){
                var satellite = data[i]; 
                var opt = document.createElement('option');
                opt.dataset.norad = satellite.norad;
                opt.dataset.name = satellite.name;
                opt.dataset.satId=satellite.sat_id;
                opt.value=satellite.norad;
                opt.innerText=`${satellite.norad} - ${satellite.name}`;
                select.appendChild(opt);
            }
            $('.selectpicker').selectpicker('refresh');
        });
    }
    
    function show_alert(type, msg) {
        var alert_container = $('#alert-messages');
        if (!alert_container.find('.row').length) {
            alert_container.append('<div class="row"></div>');
        }
        $('#alert-messages .row').prepend(
            `<div class="alert alert-${type} alert-dismissible" role="alert">
              ${msg}
              <button type="button" class="btn btm-sm btn-without-border-red" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true" class="bi bi-x"></span>
              </button>
            </div>`);
        setTimeout(function() {
            $('#alert-messages .row .alert:not(.disappear)').last().addClass('disappear');
            setTimeout(function() {
                $('#alert-messages .row .alert').last().remove();
            }, 1200);
        }, 5000);
    }
    
    $('#second_schedule').click(function() {
        $(this).addClass('active');
        $('#collapsePrimary').addClass('d-none');
        $('#primary_schedule').removeClass('active');
        $('#collapseSecond').removeClass('d-none');
    });

    $('#advanced-options_sec').click(function() {
        $(this).toggleClass('show');
        $('.more-options_sec').slideToggle();
    });

    $('#satellite-selection_sec').on('changed.bs.select', function ()  {
        var satellite = $(this).find(':selected').data('norad');
        var station = $('#station-info').data('id');
        select_proper_transmitters({
            satellite: satellite,
            station: station
        });
    });
    
    $('#add-button_sec').on('click',function () {
        $('#satellite-table_sec tbody .empty-schedule').remove();
        var table_body = $('#satellite-table_sec tbody')[0];
        var sat_id=$('#satellite-selection_sec').find(':selected').data('sat-id');
        var norad_cat_id=$('#satellite-selection_sec').find(':selected').data('norad');
        var sat_name=$('#satellite-selection_sec').find(':selected').data('name');
        var row = table_body.insertRow(0);
        row.className='ui-sortable-handle';
        var satellite_name_cell = row.insertCell(0);
        var transmitter_uuid_cell = row.insertCell(1);
        var transmitter_freq=row.insertCell(2);
        var transmitter_mode=row.insertCell(3);
        var button=row.insertCell(4);
        satellite_name_cell.innerHTML = `<a href="#" data-toggle="modal" data-target="#SatelliteModal" data-id=${norad_cat_id} data-sat-id=${sat_id}>${sat_name}</a>`;
        transmitter_uuid_cell.innerText = $('#transmitter-selection_sec').find(':selected').data('uuid');
        transmitter_freq.innerText = format_frequency($('#transmitter-selection_sec').find(':selected').data('downlink-low'));
        transmitter_mode.innerText = $('#transmitter-selection_sec').find(':selected').data('downlink-mode');
        satellite_name_cell.dataset.sat_id=sat_id;
        satellite_name_cell.dataset.norad_cat_id=norad_cat_id;
        button.innerHTML='<button type="button" class="btn btm-sm btn-without-border-red delete-row-button" aria-hidden="true">&times;</button>';
        button.addEventListener('click',function(event) {
            if(!confirm(gettext('Вы правда хотите удалить этот спутник??'))) {return;} 
            var $closest = event.target.closest('tr');
            $closest.remove();
            save_schedule(); 
        });
        $('#add-button_sec').prop('disabled',true);
        save_schedule();
        unlock_schedule_status_button();   
    });
    
    $('.delete-row-button_sec').on('click',function(event) {
        var mes = gettext('Вы правда хотите удалить этот спутник из расписания?');
        if(!confirm(mes)) {return;} 
        var $closest = event.target.closest('tr');
        $closest.remove();
        let  table = $('#satellite-table_sec');
        $('td.index', table).each(function (i) {
            $(this).html(i+1);
        });
        $('input[type=text]', table).each(function (i) {
            $(this).val(i + 1);
        });
        save_schedule(); 
    });
    
    $('#collapseSecond [name="status_sec"]').click(function () {
        $(this).parent().addClass('active');
        $('#collapseSecond [name="status_sec"]:not(:checked)').parent().removeClass('active');
        save_status();
    });

    function update_add_satellite_button_status(){
        //check if satellite have transmitters in frequency range
        if ($('#transmitter-selection_sec').val())
        {
            $('#add-button_sec').prop('disabled',false);
        }else
        {
            $('#add-button_sec').prop('disabled',true);
        }
        // check if this transmitter is already in schedule
        var table = document.querySelectorAll('#satellite-table_sec tbody tr');
        var transmitter_uuid= $('#transmitter-selection_sec').find(':selected').data('uuid');
        table.forEach(function(tr){
            if (tr.querySelectorAll('td')[1].innerText==transmitter_uuid)
            {
                $('#add-button_sec').prop('disabled',true);
                var error_msg = gettext('Этот передатчик уже добавлен в дополнительное расписание');
                show_alert('error', error_msg);
            }
            
        });
    }
    
    function select_proper_transmitters(filters) {
        var url = '/transmitters_list/';
        var data = { 'satellite': filters.satellite };
        if (filters.station) {
            data.station_id = filters.station;
        }
        $.ajax({
            url: url,
            data: data,
            dataType: 'json',
        }).done(function (data) {
            if (data.length == 1 && data[0].error) {
                var no_transmitter_text=gettext('Нет доступных передатчиков');
                $('#transmitter-selection_sec').html(`<option id="no-transmitter"
                                                          value="" selected>
                                                    ${no_transmitter_text}
                                                  </option>`).prop('disabled', true);
                $('s').selectpicker('refresh');
                update_add_satellite_button_status();
            } else if (data.transmitters_active.length > 0 || data.transmitters_inactive.length > 0 || data.transmitters_unconfirmed.length > 0) {
                var transmitters_options = '';
                var inactive_transmitters_options = '';
                var unconfirmed_transmitters_options = '';
                var hidden_input = '';
                var transmitter;
                if (filters.transmitter) {
                    var is_transmitter_available_active = (data.transmitters_active.findIndex(tr => tr.uuid == filters.transmitter) > -1);
                    if (is_transmitter_available_active) {
                        transmitter = data.transmitters_active.find(tr => tr.uuid == filters.transmitter);
                        transmitters_options = create_transmitter_option(filters.satellite, transmitter);
                        $('#transmitter-selection_sec').html(transmitters_options);
                        $('#transmitter-selection_sec').selectpicker('val', filters.transmitter);
                        hidden_input = `<input type="hidden" name="transmitter" value="${filters.transmitter}">`;
                        $('#transmitter-selection_sec').after(hidden_input);
                        filters.transmitter = transmitter.uuid;
                    } else {
                        var is_transmitter_available_inactive = (data.transmitters_inactive.findIndex(tr => tr.uuid == filters.transmitter) > -1);
                        if (is_transmitter_available_inactive) {
                            transmitter = data.transmitters_inactive.find(tr => tr.uuid == filters.transmitter);
                            transmitters_options = create_transmitter_option(filters.satellite, transmitter);
                            $('#transmitter-selection_sec').html(transmitters_options);
                            $('#transmitter-selection_sec').selectpicker('val', filters.transmitter);
                            hidden_input = `<input type="hidden" name="transmitter" value="${filters.transmitter}">`;
                            $('#transmitter-selection_sec').after(hidden_input);
                            filters.transmitter = transmitter.uuid;
                        } else {
                            var is_transmitter_available_unconfirmed = (data.transmitters_unconfirmed.findIndex(tr => tr.uuid == filters.transmitter) > -1);
                            if (is_transmitter_available_unconfirmed) {
                                transmitter = data.transmitters_unconfirmed.find(tr => tr.uuid == filters.transmitter);
                                transmitters_options = create_transmitter_option(filters.satellite, transmitter);
                                $('#transmitter-selection_sec').html(transmitters_options);
                                $('#transmitter-selection_sec').selectpicker('val', filters.transmitter);
                                hidden_input = `<input type="hidden" name="transmitter" value="${filters.transmitter}">`;
                                $('#transmitter-selection_sec').after(hidden_input);
                                filters.transmitter = transmitter.uuid;
                            }
                            else {
                                $('#transmitter-selection_sec').html(`<option id="no-transmitter" value="" selected>
                                ${no_transmitter_text}
                            </option>`).prop('disabled', true);
                                delete filters.transmitter;
                                update_add_satellite_button_status();
                            }
                        }
                    }
                    $('#transmitter-selection_sec').selectpicker('refresh');
                } else {
                    var max_good_count = 0;
                    var max_good_val = '';
                    $.each(data.transmitters_active, function (i, transmitter) {
                        if (max_good_count <= transmitter.good_count) {
                            max_good_count = transmitter.good_count;
                            max_good_val = transmitter.uuid;
                        }
                        transmitters_options += create_transmitter_option(filters.satellite, transmitter);
                    });
                    var inactive_max_good_count = 0;
                    var inactive_max_good_val = '';
                    $.each(data.transmitters_inactive, function (i, transmitter) {
                        if (!max_good_count && inactive_max_good_count <= transmitter.good_count) {
                            inactive_max_good_count = transmitter.good_count;
                            inactive_max_good_val = transmitter.uuid;
                        }
                        inactive_transmitters_options += create_transmitter_option(filters.satellite, transmitter);
                    });
                    var unconfirmed_max_good_count = 0;
                    var unconfirmed_max_good_val = '';
                    $.each(data.transmitters_unconfirmed, function (i, transmitter) {
                        if ((!max_good_count || inactive_max_good_count) && unconfirmed_max_good_count <= transmitter.good_count) {
                            unconfirmed_max_good_count = transmitter.good_count;
                            unconfirmed_max_good_val = transmitter.uuid;
                        }
                        unconfirmed_transmitters_options += create_transmitter_option(filters.satellite, transmitter);
                    });
                    var active_text=gettext('Активные');
                    var inactive_text=gettext('Неактивные');
                    var unconfirmed_text=gettext('Неподтвержденные');
                    if (transmitters_options) {
                        transmitters_options = `<optgroup label="${active_text}">${transmitters_options}</optgroup>`;
                    }

                    if (inactive_transmitters_options) {
                        inactive_transmitters_options = `<optgroup label="${inactive_text}>${inactive_transmitters_options}</optgroup>`;
                    }

                    if (unconfirmed_transmitters_options) {
                        unconfirmed_transmitters_options = `<optgroup label=${unconfirmed_text}>${unconfirmed_transmitters_options}</optgroup>`;
                    }

                    $('#transmitter-selection_sec').html(transmitters_options + inactive_transmitters_options + unconfirmed_transmitters_options).prop('disabled', false);

                    $('#transmitter-selection_sec').selectpicker('refresh');
                    $('#transmitter-selection_sec').selectpicker('val', max_good_val || inactive_max_good_val || unconfirmed_max_good_val);
                    filters.transmitter = max_good_val || inactive_max_good_val || unconfirmed_max_good_val;
                }
                $('.tle').hide();
                $('.tle[data-norad="' + filters.satellite + '"]').show();
            } else {
                $('#transmitter-selection_sec').html(`<option id="no-transmitter"
                                                          value="" selected>
                                                    ${no_transmitter_text}
                                                  </option>`).prop('disabled', true);
                $('#transmitter-selection_sec').selectpicker('refresh');
                $('#station-field-loading').hide();
                $('#station-field').show();
            }
            $('#transmitter-field-loading').hide();
            $('#transmitter-field').show();
            update_add_satellite_button_status();
        });
    }
    
    function create_transmitter_option(satellite, transmitter) {
        const transmitter_freq = (transmitter.type === 'Transponder') ? (transmitter.downlink_low / 1e6).toFixed(3) + ' - ' + (transmitter.downlink_high / 1e6).toFixed(3) : (transmitter.downlink_low / 1e6).toFixed(3);
        var good_text=gettext('Хорошие');
        var unknown_text=gettext('Неизвестные');
        var bad_text=gettext('Плохие');
        var future_text=gettext('Будущие');
        var freq_text=gettext('МГц');
        return `
            <option data-satellite="${satellite}"
                    data-transmitter-type="${transmitter.type}"
                    data-downlink-low="${transmitter.downlink_low}"
                    data-downlink-mode="${transmitter.downlink_mode}"
                    data-downlink-high="${transmitter.downlink_high}"
                    data-downlink-drift="${transmitter.downlink_drift}"
                    data-uuid="${transmitter.uuid}"
                    value="${transmitter.uuid}"
                    data-success-rate="${transmitter.success_rate}"
                    data-content='<div class="transmitter-option">
                                    <div class="transmitter-description">
                                      ${transmitter.description} | ${transmitter_freq} ${freq_text} | ${transmitter.downlink_mode}
                                    </div>
                                    <div class="progress">
                                      <div class="progress-bar pb-green transmitter-good"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="${transmitter.success_rate}% (${transmitter.good_count}) ${good_text}"
                                        style="width:${transmitter.success_rate}%"></div>
                                      <div class="progress-bar pb-orange transmitter-unknown"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="${transmitter.unknown_rate}% (${transmitter.unknown_count}) ${unknown_text}"
                                        style="width:${transmitter.unknown_rate}%"></div>
                                      <div class="progress-bar pb-red transmitter-bad"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="${transmitter.bad_rate}% (${transmitter.bad_count}) ${bad_text}"
                                        style="width:` + transmitter.bad_rate + `%"></div>
                                      <div class="progress-bar pb-blue transmitter-future"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="${transmitter.future_rate}% (${transmitter.future_count}) ${future_text}"
                                        style="width:${transmitter.future_rate}%"></div>
                                    </div>
                                  </div>'>
            </option>
        `;
    }

    function format_frequency(val) {
        if (!Number.isInteger(val)) {
            return gettext('Ошибка');
        }
        
        const unit_table = [gettext('Гц'), gettext('КГц'), gettext('МГц'), gettext('ГГц')];
        const div = Math.floor(Math.log10(val) / 3);
        const unit = unit_table[(div > 3) ? 3 : div];
        
        return val / (Math.pow(1000, (div > 3) ? 3 : div)) + ' ' + unit;
    }
    const frequency_input_format = $('#frequency-input-format');
    const frequency_input = $('#center-frequency-input');
    const frequency_formgroup = $('#center-frequency-formgroup');
    const frequency_errors = { '1': gettext('Значение не число'), '2': gettext('Значение вне диапазона') };
    
    frequency_input.on('input', function () {
        var val = parseInt($(this).val());
        const min = parseInt(frequency_input.attr('min'));
        const max = parseInt(frequency_input.attr('max'));
        var has_error = 0;
        
        if (isNaN(val)) {
            has_error = 1;
        }
        else if (val < min || val > max) {
            has_error = 2;
        }

        if (!has_error) {
            frequency_formgroup.removeClass('has-error');
            frequency_input_format.removeClass('alert-error');
            frequency_input.data('is-valid', true);
            frequency_input_format.html(format_frequency(val));
            
        } else {
            frequency_formgroup.addClass('has-error');
            frequency_input_format.addClass('alert-error');
            frequency_input_format.html(frequency_errors[has_error]);
            
            frequency_input.data('is-valid', false);
        }
    });

    function unlock_schedule_status_button(){
        if (($('#active-status_sec').prop('disabled')==true)&&($('#inactive-status_sec').prop('disabled')==true)) {
            $('#active-status_sec').prop('disabled',false).removeAttr('disabled');
            $('#inactive-status_sec').prop('disabled',false).removeAttr('disabled');
            $('#active-status_sec').prop('checked',true);
        }
    }

    function save_schedule() {
        var url = '/save_auto_schedule_sec/';
        var data ={};
        data.station=$('#station-info').data('id');
        var satellites=[];
        var table = document.querySelectorAll('#satellite-table_sec tbody tr');
        table.forEach(function(tr){
            var row=tr.querySelectorAll('td');
            var row_data={};
            var sat_row=row[0].getElementsByTagName('a')[0];
            row_data.sat_name=sat_row.innerText;
            row_data.sat_id=sat_row.dataset.satId;
            row_data.transmitter_uuid=row[1].innerText;
            satellites.push(JSON.stringify(row_data));
        });
        data.satellites=satellites;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRFToken', $('[name="csrfmiddlewaretoken"]').val());
            }
        }).done(function (results) {
            var success_msg=results.success;
            show_alert('success', success_msg);
        } 
        ).fail(function(error){
            var error_msg=error.responseJSON.error;
            show_alert('error', error_msg);
            let table_body = $('#satellite-table_sec tbody')[0];
            table_body.deleteRow(-1);
        });
    }
    
    function save_status(){
        var url = '/save_auto_schedule_status_sec/';
        var data ={};
        data.station=$('#station-info').data('id');
        data.status=$('input[name="status_sec"]:checked').val(); 
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRFToken', $('[name="csrfmiddlewaretoken"]').val());
            }
        }).done(function (results) {
            var success_msg=results.success;
            show_alert('success', success_msg);
        }).fail(function(error){
            var error_msg=error.responseJSON.error;
            show_alert('error', error_msg);
        });
    }

    // функции для параметров
    function save_parameters(input_data){
        var url='/save_auto_schedule_parameters_sec/';
        var data=input_data;
        data.station=$('#station-info').data('id');
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRFToken', $('[name="csrfmiddlewaretoken"]').val());
            }
        }).done(function (results) {
            var success_msg=results.success;
            show_alert('success', success_msg);
            $('#set-parameters_sec').prop('disabled',true);
        }).fail(function(error){
            var error_msg=error.responseJSON.error;
            show_alert('error', error_msg);
        });
    }

    $('#default-horizon_sec').click(function () {
        $('#horizon-status_sec .slider-wrapper').remove();
        $('#set-parameters_sec').prop('disabled',false);
        $(this).addClass('active');
        $(this).next().removeClass('active');
    });
    
    $('#custom-horizon_sec').click(function () {
        $(this).addClass('active');
        $(this).prev().removeClass('active');
        if ($('#min-horizon_sec').length == 0) {
            var horizon_default_value=$('#default-horizon_sec input').val();
            $('#horizon-status_sec').append('<div class="slider-wrapper"><input type="hidden" name="min-horizon_sec" id="min-horizon_sec"></div>');
            new Slider('#min-horizon_sec',{id:'min-horizon-slider_sec',
                min:0,
                max:90,
                step:1,
                value:parseInt(horizon_default_value),
                ticks:[0, 30, 60, 90],
                ticks_labels: ['0', '30', '60', '90'],
                ticks_positions:[0,33,66,100]
            }).on('change',function(){
                $('#set-parameters_sec').prop('disabled',false);
            });
        }
    });
    
    $('#default-culmination_sec').click(function () {
        $('#culmination-status_sec .slider-wrapper').remove();
        $('#set-parameters_sec').prop('disabled',false);
        $(this).addClass('active');
        $(this).next().removeClass('active');
    });
    
    $('#custom-culmination_sec').click(function () {
        $(this).addClass('active');
        $(this).prev().removeClass('active');
        if ($('#max-culmination_sec').length == 0) {
            $('#culmination-status_sec').append('<div class="slider-wrapper"><input type="hidden" name="max-culmination_sec" id="max-culmination_sec"></div>');
            new Slider('#max-culmination_sec',{id:'max-culmination-slider_sec',
                min:0,
                max:90,
                step:1,
                value:[0,90],
                ticks:[0, 30, 60, 90],
                ticks_labels: ['0', '30', '60', '90'],
                ticks_positions:[0,33,66,100]
            }).on('change',function(){
                $('#set-parameters_sec').prop('disabled',false);
            });
        }
    });
    
    const custom_split_formgroup = $('#split-duration-formgroup_sec');
    function reset_split_duration() {
        custom_split_formgroup.removeClass('has-error');
        $('#split-duration-custom_sec').remove();
        $('#split-duration-span_sec').remove();
    }
    
    $('#default-split-duration_sec').click(function () {
        reset_split_duration();
        $(this).addClass('active');
        $(this).next().removeClass('active');
        $('#set-parameters_sec').prop('disabled',false);
    });

    function get_errors(code, min_val) {
        switch (code) {
        case 1:
            return gettext('Значение не число');
        case 2:
            return `${gettext('Введите значение равное и больше, чем')} ${min_val}.`;
        default:
            return gettext('Некорректный ввод.');
        }
    }

    $('#custom-split-duration_sec').click(function () {
        if ($('#split-duration-custom_sec').length == 0) {
            $(this).addClass('active');
            $(this).prev().removeClass('active');
            var value = $('#default-split-duration_sec input')[0].value;
            var min_value = $('#default-split-duration_sec input')[0].dataset.min;
            $('#split-duration-status_sec').append('<input type="number" name="split_duration_custom_sec" id="split-duration-custom_sec" class="duration-number-input form-control w-100" min="' + min_value + '" step="1" value="' + value + '">');
            $('#split-duration-status_sec').append('<span id="split-duration-span_sec"></span>');
            const custom_split = $('#split-duration-custom_sec');
            custom_split.on('input',function(){
                $('#set-parameters_sec').prop('disabled',false);
            });
            custom_split.on('input', function () {
                var has_error = 0;
                if (isNaN(custom_split.val()) || custom_split.val() == '') {
                    has_error = 1;
                } else if (parseInt(custom_split.val()) < min_value) {
                    has_error = 2;
                }
                if (has_error) {
                    custom_split_formgroup.addClass('has-error');
                    $('#set-parameterss_sec').prop('disabled', true);
                    $('#split-duration-span_sec').addClass('alert-error');
                    $('#split-duration-span_sec').html(get_errors(has_error, min_value));
                } else {
                    custom_split_formgroup.removeClass('has-error');
                    $('#set-parameterss_sec').prop('disabled', false);
                    $('#split-duration-span_sec').removeClass('alert-error');
                    $('#split-duration-span_sec').html('');
                }
            });
        }
    });
    
    $('#default-break-duration_sec').click(function () {
        $('#break-duration-custom_sec').remove();
        $('#set-parameters_sec').prop('disabled',false);
        $(this).addClass('active');
        $(this).next().removeClass('active');
    });
    
    $('#custom-break-duration_sec').click(function () {
        $(this).addClass('active');
        $(this).prev().removeClass('active');
        if ($('#break-duration-custom_sec').length == 0) {
            var value = $('#default-break-duration_sec input')[0].value;
            $('#break-duration-status_sec').append('<input type="number" name="break_duration_custom_sec" id="break-duration-custom_sec" class="duration-number-input form-control w-100" min="0" step="1" value="' + value + '">');
            $('#break-duration-status_sec').append('<span id="break-duration-span_sec"></span>');
            $('#break-duration-custom_sec').on('input',function(){
                $('#set-parameters_sec').prop('disabled',false);
            });
            var min_value = $('#break-duration-custom_sec').attr('min');
            $('#break-duration-custom_sec').on('input', function() {
                var has_error = 0;
                if (isNaN($(this).val()) || $(this).val() == '') {
                    has_error = 1;
                } else if (parseInt($(this).val()) < min_value) {
                    has_error = 2;
                }
                if (has_error) {
                    $('#set-parameters_sec').prop('disabled',true);
                    $('#break-duration-span_sec').addClass('alert-error');
                    $('#break-duration-span_sec').html(get_errors(has_error, min_value));
                } else {
                    $('#set-parameters_sec').prop('disabled',false);
                    $('#break-duration-span_sec').removeClass('alert-error');
                    $('#break-duration-span_sec').html('');
                }
            });
        }
    });

    $('#default-azimutes_sec').click(function () {
        $('#azimuts_sec .slider-wrapper').remove();
        $('#set-parameters_sec').prop('disabled',false);
        $(this).addClass('active');
        $(this).next().removeClass('active');
    });

    $('#custom-azimutes_sec').click(function () {
        $(this).addClass('active');
        $(this).prev().removeClass('active');
        if ($('#min-max-azimutes_sec').length == 0) {
            $('#azimuts_sec').append('<div class="slider-wrapper"><input type="hidden" name="min-max-azimutes_sec" id="min-max-azimutes_sec"></div>');
            new Slider('#min-max-azimutes_sec',{id:'min-max-azimutes-slider_sec',
                range:true,    
                min:0,
                max:360,
                step:1,
                value:[0,360],
                ticks:[0, 90, 180, 270,360],
                ticks_positions:[0,25,50,75,100],
                ticks_labels: ['0', '90', '180', '270','360']}).on('change',function(){
                $('#set-parameters_sec').prop('disabled',false);
            });
        }
    });
    
    $('#set-parameters_sec').click(function() {
        var data ={};
        var is_custom_horizon=$('#horizon-status_sec input[type=radio]').filter(':checked').val()=='custom';
        if (is_custom_horizon) {
            data.min_horizon=parseInt($('#min-horizon_sec').val());
        }
        else {
            data.min_horizon=parseInt($('#horizon-status_sec input[type=radio]').filter(':checked').val());
        }
        var is_split_duration = $('#split-duration-status_sec input[type=radio]').filter(':checked').val() == 'custom';
        if (is_split_duration) {
            var split_duration = parseInt($('#split-duration-custom_sec').val());
            var min_value = parseInt($('#default-split-duration_sec input')[0].dataset.min);
            if (!isNaN(split_duration) && split_duration >= min_value) {
                data.split_duration = split_duration;
            }
        }
        else {
            data.split_duration = parseInt($('#split-duration-status_sec input[type=radio]').filter(':checked').val());
        }
        var is_break_duration = $('#break-duration-status_sec input[type=radio]').filter(':checked').val() == 'custom';
        if (is_break_duration) {
            var break_duration = parseInt($('#break-duration-custom_sec').val());
            if (!isNaN(break_duration) && break_duration > 0) {
                data.break_duration = break_duration;
            }
            else if (break_duration < 0) {
                data.break_duration = 0;
            }
        }
        else {
            data.break_duration = parseInt($('#break-duration-status_sec input[type=radio]').filter(':checked').val());
        }
        
        var is_custom_azimutes= $('#azimuts_sec input[type=radio]').filter(':checked').val() == 'custom';
        if (is_custom_azimutes) {
            var list_az=$('#min-max-azimutes_sec').val().split(',');
            data.start_azimuth = parseInt(list_az[0]);
            data.end_azimuth = parseInt(list_az[1]);
        }
        else {
            var default_list_az=$('#default-azimutes_sec input[type=radio]').filter(':checked').val().split(',');
            data.start_azimuth=parseInt(default_list_az[0]);
            data.end_azimuth=parseInt(default_list_az[1]);
        }
        var is_custom_elevation=$('#culmination-status_sec input[type=radio]').filter(':checked').val()=='custom';
        if (is_custom_elevation) {
            if (is_custom_elevation) {
                var list_cul=$('#max-culmination_sec').val().split(',');
                data.start_culmination = parseInt(list_cul[0]);
                data.end_culmination = parseInt(list_cul[1]);
            }
        }
        else {
            var default_list_cul=$('#culmination-status_sec input[type=radio]').filter(':checked').val().split(',');
            data.start_culmination=parseInt(default_list_cul[0]);
            data.end_culmination=parseInt(default_list_cul[1]);        
        }
        save_parameters(data);
    });
    
    function check_if_have_parameters(station_id){
        var url = '/parameters_list_sec/';
        var data= { 'station': station_id };
        $.ajax({
            url: url,
            data: data,
            dataType: 'json',
        }).done(function (data) {
            if (data==null) {
                data={};
            }

            if ('min_horizon' in data) {
                $('#custom-horizon_sec').addClass('active');
                $('#default-horizon_sec').removeClass('active');
                ($('input[name="horizon_sec"][value="custom"]')).prop('checked',true);
                $('#horizon-status_sec').append('<div class="slider-wrapper"><input type="hidden" name="min-horizon_sec" id="min-horizon_sec"></div>');
                new Slider('#min-horizon_sec',{id:'min-horizon-slider_sec',
                    min:0,
                    max:90,
                    step:1,
                    value:parseInt(data.min_horizon),
                    ticks:[0, 30, 60, 90],
                    ticks_labels: ['0', '30', '60', '90'],
                    ticks_positions:[0,33,66,100]}).on('change',function(){
                    $('#set-parameters_sec').prop('disabled',false);
                });
            }
            else {
                $('#default-horizon_sec').addClass('active');
                ($('#default-horizon_sec input[name="horizon_sec"]')).prop('checked',true);
            }
            if ('start_culmination' in data || 'end_culmination' in data ) {
                $('#custom-culmination_sec').addClass('active');
                $('#default-culmination_sec').removeClass('active');
                ($('input[name="culmination_sec"][value="custom"]')).prop('checked',true);
                $('#culmination-status_sec').append('<div class="slider-wrapper"><input type="hidden" name="max-culmination_sec" id="max-culmination_sec"></div>');
                new Slider('#max-culmination_sec',{id:'max-culmination-slider_sec',
                    min:0,
                    max:90,
                    step:1,
                    value:[parseInt(data.start_culmination)||0,parseInt(data.end_culmination)||90],
                    ticks:[0, 30, 60, 90],
                    ticks_labels: ['0', '30', '60', '90'],
                    ticks_positions:[0,33,66,100]}).on('change',function(){
                    $('#set-parameters_sec').prop('disabled',false);
                });
            }
            else {
                $('#default-culmination_sec').addClass('active');
                ($('#default-culmination_sec input[name="culmination_sec"]')).prop('checked',true);
            }

            if ('split' in data) {
                $('#custom-split-duration_sec').addClass('active');
                $('#default-split-duration_sec').removeClass('active');
                ($('input[name="split_duration_sec"][value="custom"]')).prop('checked',true);
                var split_value = parseInt(data.split);
                var min_value = $('#default-split-duration_sec input')[0].dataset.min;
                $('#split-duration-status_sec').append('<input type="number" name="split_duration_custom_sec" id="split-duration-custom_sec" class="duration-number-input form-control w-100" min="' + min_value + '" step="1" value="' + split_value + '">');
                $('#split-duration-status_sec').append('<span id="split-duration-span_sec"></span>');
                const custom_split = $('#split-duration-custom_sec').on('change',function(){
                    $('#set-parameters_sec').prop('disabled',false);
                });
                custom_split.on('input', function () {
                    var has_error = 0;
                    if (isNaN(custom_split.val()) || custom_split.val() == '') {
                        has_error = 1;
                    } else if (parseInt(custom_split.val()) < min_value) {
                        has_error = 2;
                    }
    
                    if (has_error) {
                        custom_split_formgroup.addClass('has-error');
                        $('#set-parameters_sec').prop('disabled', true);
                        $('#split-duration-span_sec').addClass('alert-error');
                        $('#split-duration-span_sec').html(get_errors(has_error, min_value));
                    } else {
                        custom_split_formgroup.removeClass('has-error');
                        $('#set-parameters_sec').prop('disabled', false);
                        $('#split-duration-span_sec').removeClass('alert-error');
                        $('#split-duration-span_sec').html('');
                    }
                });
            }
            else {
                $('#default-split-duration_sec').addClass('active');
                ($('#default-split-duration_sec input[name="split_duration_sec"]')).prop('checked',true);
            }

            if ('break' in data) {
                $('#custom-break-duration_sec').addClass('active');
                $('#default-break-duration_sec').removeClass('active');
                ($('input[name="break_duration_sec"][value="custom"]')).prop('checked',true);
                var break_value = parseInt(data.break);
                min_value = $('#break-duration-custom_sec').attr('min');
                $('#break-duration-status_sec').append('<span id="break-duration-span_sec"></span>');
                $('#break-duration-status_sec').append('<input type="number" name="break_duration_custom_sec" id="break-duration-custom_sec" class="duration-number-input form-control w-100" min="0" step="1" value="' + break_value + '">')
                    .on('input',function(){
                        $('#set-parameters_sec').prop('disabled',false);
                    });
                $('#break-duration-custom_sec').on('input', function() {
                    var has_error = 0;
                    if (isNaN($(this).val()) || $(this).val() == '') {
                        has_error = 1;
                    } else if (parseInt($(this).val()) < min_value) {
                        has_error = 2;
                    }
                    if (has_error) {
                        $('#set-parameters_sec').prop('disabled',true);
                        $('#break-duration-span_sec').addClass('alert-error');
                        $('#break-duration-span_sec').html(get_errors(has_error, min_value));
                    } else {
                        $('#set-parameters_sec').prop('disabled',false);
                        $('#break-duration-span_sec').removeClass('alert-error');
                        $('#break-duration-span_sec').html('');
                    }
                });
            }
            else {
                $('#default-break-duration_sec').addClass('active');
                ($('#default-break-duration_sec input[name="break_duration_sec"]')).prop('checked',true);
            }

            if ('min_az' in data || 'max_az' in data ) {
                $('#custom-azimutes_sec').addClass('active');
                $('#default-azimutes_sec').removeClass('active');
                ($('input[name="azimutes_sec"][value="custom"]')).prop('checked',true);
                $('#azimuts_sec').append('<div class="slider-wrapper"><input type="hidden" name="min-max-azimutes_sec" id="min-max-azimutes_sec"></div>');
                new Slider('#min-max-azimutes_sec',{id:'min-max-azimutes-slider_sec',  
                    min:0,
                    max:360,
                    step:1,
                    value:[parseInt(data.min_az)||0,parseInt(data.max_az)||360],
                    ticks:[0, 90, 180, 270,360],
                    ticks_positions:[0,25,50,75,100],
                    ticks_labels: ['0', '90', '180', '270','360']}).on('change',function(){
                    $('#set-parameters_sec').prop('disabled',false);
                });
            }
            else {
                $('#default-azimutes_sec').addClass('active');
                ($('#default-azimutes_sec input[name="azimutes_sec"]')).prop('checked',true);
            }
        }).fail(function(){
            $('#default-horizon_sec').addClass('active');
            ($('#default-horizon_sec input[name="horizon_sec"]')).prop('checked',true);
            $('#default-culmination_sec').addClass('active');
            ($('#default-culmination_sec input[name="culmination_sec"]')).prop('checked',true);
            $('#default-split-duration_sec').addClass('active');
            ($('#default-split-duration_sec input[name="split_duration_sec"]')).prop('checked',true);
            $('#default-break-duration_sec').addClass('active');
            ($('#default-break-duration_sec input[name="break_duration_sec"]')).prop('checked',true);
            $('#default-azimutes_sec').addClass('active');
            ($('#default-azimutes_sec input[name="azimutes_sec"]')).prop('checked',true);
        }); 
    }
    
    check_if_have_parameters($('#station-info').data('id'));
    get_satellites();
});
