/* global  calcPolarPlotSVG, gettext, Fancybox */

$(document).ready(observationView);
document.addEventListener('obs_changed', observationView, false);

function observationView() {
    'use strict';

    var tabIndex = window.location.href.split('#');
    if (tabIndex.length > 1) {
        tabIndex = tabIndex.at(-1).replace('/', '');
        var currentTab = $('#observationTabContent #' + tabIndex);
        if (currentTab.length) {
            $('#observationTabContent .tab-pane').removeClass('active');
            currentTab.addClass('active');
            $('.nav-tabs li').removeClass('active');
            $('a[data-target-tab=' + tabIndex).parent().addClass('active');
            window.location.hash = '#' + tabIndex;
            if (window.innerWidth > 1199) {
                window.scrollTo(0, 220);
            }
            else {
                window.scrollBy(0, -100);
            }
        }
    }

    // btn-copy TLE
    var btnCopy = document.getElementsByClassName('btn-copy');
    for (var i = 0; i < btnCopy.length; i++){
        btnCopy[i].addEventListener('click', event => {
            var copyTarget = event.target.getAttribute('data-target');
            var copyText = document.getElementById(copyTarget).innerHTML;
            copyText = copyText.replace('<br>', '\n');
            copyFunction(copyText);
        });
    }

    const viewer = document.querySelectorAll('json-viewer');
    for (let view of viewer){
        view.expand('**');
    }

    let obs_vetted = new Event('obs_vetted');


    // Set width for not selected tabs
    var panelWidth = $('.tab-content').first().width();
    $('.tab-pane').css('width', panelWidth);
    // Format time for the player


    // Handle Observation tabs
    var uri = new URL(location.href);
    var tab = uri.hash;
    $('.observation-tabs li a[href="' + tab + '"]').tab('show');

    // Delete confirmation
    var message = gettext('Вы действительно хотите удалить данное наблюдение?');
    var actions = $('#obs-delete');
    if (actions.length) {
        actions[0].addEventListener('click', function (e) {
            if (!confirm(message)) {
                e.preventDefault();
            }
        });
    }
    //Vetting help functions
    function show_alert(type, msg) {
        var alert_container = $('#alert-messages');
        if (!alert_container.find('.row').length) {
            alert_container.append('<div class="row"></div>');
        }
        $('#alert-messages .row').append(
            `<div class="alert alert-${type} alert-dismissible" role="alert">
              ${msg}
              <button type="button" class="btn btm-sm btn-without-border-red" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true" class="bi bi-x"></span>
              </button>
            </div>`);
    }

    var status_list = {
        'Unknown': 'Неизвестный',
        'With Signal': 'Есть сигнал',
        'Without Signal': 'Нет сигнала',
        'Good': 'Хороший',
        'Bad': 'Плохой'
    };

    function change_vetting_badges(user, datetime, waterfall_status_badge, waterfall_status_display,
        status, status_badge, status_display) {
        $('#waterfall-status-form').find('button').each(function () {
            if (this.dataset.status == waterfall_status_badge) {
                $(this).addClass('d-none');
            } else {
                $(this).removeClass('d-none');
            }
        });

        var waterfall_badge_classes = 'badget-orange badget-green badget-red';

        $('#waterfall-status-badge').removeClass(waterfall_badge_classes);
        if (waterfall_status_badge == 'with-signal') {
            $('#waterfall-status-badge').addClass('badget-green').text(gettext('Есть сигнал'));
            $('#waterfall-tab .status-br').removeClass('status-error status-unknown').addClass('status-success').prop('title', gettext('Успешный'));
        }
        if (waterfall_status_badge == 'without-signal') {
            $('#waterfall-status-badge').addClass('badget-red').text(gettext('Нет сигнала'));
            $('#waterfall-tab .status-br').removeClass('status-success status-unknown').addClass('status-error').prop('title', gettext('Плохой'));
        }
        if (waterfall_status_badge == 'unknown') {
            $('#waterfall-status-badge').addClass('badget-orange').text(gettext('Неизвестно'));
            $('#waterfall-tab .status-br').removeClass('status-error status-success').addClass('status-unknown').prop('title', gettext('Неизвестный'));
        }
        var waterfall_status_title = `${user}, ${datetime}`;
        if (waterfall_status_badge == 'unknown') {
            waterfall_status_title = gettext('Водопад нужно оценить');
            if (user && datetime) {
                waterfall_status_title += ` (${user}, ${datetime})`;
            }
        }
        $('#waterfall-status-badge').prop('title', waterfall_status_title).tooltip('_fixTitle');

        var rating_badge_classes = 'badge-unknown badge-future badge-good badge-bad badge-failed';
        $('#rating-status span').removeClass(rating_badge_classes).addClass('badge-' + status_badge);
        $('#rating-status span').text(status_display);
        var status_title = status;
        $('#rating-status span').prop('title', status_title);
    }

    function handling_vetting_elements(response) {
        if (response) {
            $('#vetting-spinner').removeClass('d-inline-block');
            $('#rating-spinner').hide();
            $('#waterfall-status-badge').show();
            $('#waterfall-status-form').show();
            $('#rating-status').show();
        } else {
            $('#waterfall-status-badge').hide();
            $('#waterfall-status-form').hide();
            $('#rating-status').hide();
            $('#vetting-spinner').addClass('d-inline-block');
            $('#rating-spinner').show();
        }
    }

    //Vetting request
    function vet_waterfall(id, vet_status) {
        var data = {};
        data.status = vet_status;
        var url = '/waterfall_vet/' + id + '/';
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRFToken', $('[name="csrfmiddlewaretoken"]').val());
                handling_vetting_elements(false);
            }
        }).done(function (results) {
            if (Object.prototype.hasOwnProperty.call(results, 'error')) {
                var error_msg = results.error;
                show_alert('danger', error_msg);
                handling_vetting_elements(true);
                return;
            }
            var allert_msg = `${gettext('Водопад оценен как')} "${status_list[results.waterfall_status_display]}" ${gettext('и статус наблюдения изменен на')} "${status_list[results.status_display]}"`;
            show_alert('success', allert_msg);
            change_vetting_badges(results.waterfall_status_user, results.waterfall_status_datetime,
                results.waterfall_status_badge, results.waterfall_status_display,
                results.status, results.status_badge,
                results.status_display);
            handling_vetting_elements(true);
            document.dispatchEvent(obs_vetted);
            return;
        }).fail(function () {
            var error_msg = gettext('Что-то пошло не так. Попробуйте снова.');
            show_alert('danger', error_msg);
            handling_vetting_elements(true);
            return;
        });
    }

    $('#waterfall-status-form button:not(.btn-info)').click(function () {
        var vet_status = $(this).data('status');
        var id = $(this).data('id');
        $(this).blur();
        vet_waterfall(id, vet_status);
    });

    $('#waterfall-status-form button.btn-info').click(function() {
        $('.vet-info').slideToggle();
    });

    // Draw orbit in polar plot
    $('svg#polar').show(function () {
        var tleLine1 = $('svg#polar').data('tle1');
        var tleLine2 = $('svg#polar').data('tle2');

        var timeframe = {
            start: new Date($('svg#polar').data('timeframe-start')),
            end: new Date($('svg#polar').data('timeframe-end'))
        };

        var groundstation = {
            lon: $('svg#polar').data('groundstation-lon'),
            lat: $('svg#polar').data('groundstation-lat'),
            alt: $('svg#polar').data('groundstation-alt')
        };

        const polarPlotSVGPromise = new Promise(function (resolve) {
            resolve(calcPolarPlotSVG(timeframe,
                groundstation,
                tleLine1,
                tleLine2
            ));
        });

        polarPlotSVGPromise.then((polarPlotSVG) => {
            $('svg#polar').append(polarPlotSVG);
            $('svg#polar path:last').css('stroke', '#0097ff');
        });
    });

    // Return hex string from ArrayBuffer that contains DemodData bytes
    function buffer_to_hex(buffer) {
        var hex_string = '';
        var hex_digit = '0123456789ABCDEF';
        (new Uint8Array(buffer)).forEach((v) => { hex_string += hex_digit[v >> 4] + hex_digit[v & 15] + ' '; });
        return hex_string.slice(0, -1);
    }

    // Load dynamicaly DemodData files
    function load_demoddata(num, load_img) {
        $('#load-data-btn-group button').hide();
        $('#demoddata-spinner').show();
        var demoddata_to_show = $('#image-tab .demoddata:hidden');
        var number_of_divs = demoddata_to_show.length;
        var divs_shown = 0;

        if (load_img) {
            demoddata_to_show.each(function () {
                var demoddata_div = $(this);
                var url = demoddata_div.find('a').attr('href');
                var body = demoddata_div.find('.data__body');
                if (body.is(':empty')) {
                    body.append(`<img src="${url}" alt="${gettext('Принятое изображение')}" class="img-fluid">`);
                    body.find('img').on('error', function() {
                        body.empty();
                        var xhr = new XMLHttpRequest();
                        xhr.open('GET', url, true);
                        xhr.responseType = 'arraybuffer';
                        xhr.onload = function () {
                            body.append('<span class="hex">' + buffer_to_hex(this.response) + '</span>');
                        };
                        xhr.send();
                    });
                }
                demoddata_div.show();
            });
        }

        demoddata_to_show = $('#data-tab .demoddata:hidden').slice(0, num);
        number_of_divs = demoddata_to_show.length;
        demoddata_to_show.each(function () {
            var demoddata_div = $(this);
            var content_type = demoddata_div.data('type');
            var url = demoddata_div.find('a').attr('href');
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = 'arraybuffer';
            xhr.onload = function () {
                if (this.status == 200) {
                    if (content_type == 'binary') {
                        demoddata_div.find('.data__body').append('<span class="hex">' + buffer_to_hex(this.response) + '</span>');
                        var ascii_enc = new TextDecoder('ascii');
                        demoddata_div.find('.data__body').append('<span class="ascii">' + ascii_enc.decode(this.response) + '</span>');
                        let ax25_html = '';
                        try {
                            const parsedAx25 = new Ax25monitor(new KaitaiStream(this.response)); // eslint-disable-line no-undef
                            const srcCallsign = parsedAx25.ax25Frame.ax25Header.srcCallsignRaw.callsignRor.callsign;
                            const dstCallsign = parsedAx25.ax25Frame.ax25Header.destCallsignRaw.callsignRor.callsign;
                            const alphanum_with_white_spaces_regex = /^[a-z0-9-\s]+$/i;
                            /* eslint-disable quotes*/
                            ax25_html = `
                            <table class='table table-sm table-borderless ax25'>
                                <tbody>
                                <tr>
                                    <th>
                                    <span class='badge badge-secondary'>${gettext('Позывной источника')}</span>
                                    </th>
                                    <td>
                                    <span>${srcCallsign}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                    <span class='badge badge-secondary'>${gettext('Позывной назначения')}</span>
                                    </th>
                                    <td>
                                    <span>${dstCallsign}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                    <span class='badge badge-secondary'>${gettext('SSID источника')}</span>
                                    </th>
                                    <td>
                                    <span>${parsedAx25.ax25Frame.ax25Header.srcSsidRaw.ssid}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                    <span class='badge badge-secondary'>${gettext('SSID назначения')}</span>
                                    </th>
                                    <td>
                                    <span>${parsedAx25.ax25Frame.ax25Header.destSsidRaw.ssid}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                    <span class='badge badge-secondary'>Ctl</span>
                                    </th>
                                    <td>
                                    <span>${parsedAx25.ax25Frame.ax25Header.ctl}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                    <span class='badge badge-secondary'>Pid</span>
                                    </th>
                                    <td>
                                    <span>${parsedAx25.ax25Frame.payload.pid}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                    <span class='badge badge-secondary'>${gettext('Данные')}</span>
                                    </th>
                                    <td>
                                    <span>${parsedAx25.ax25Frame.payload.ax25Info.dataMonitor}</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>`;
                            /* eslint-enable quotes*/
                            if (alphanum_with_white_spaces_regex.test(srcCallsign) === true && alphanum_with_white_spaces_regex.test(dstCallsign) === true) {
                                $('#ax25-button').show();
                            }
                        } catch (error) {
                            ax25_html = `<span class="ax25">${gettext('Возникла ошибка при декодировании. Возможно, этот пакет не в формате AX.25')}</span>`;
                        }
                        demoddata_div.find('.data__body').append(ax25_html);

                        // check if currently ASCII/HEX/AX25 button is active and display accordingly
                        if ($('#hex-button').hasClass('active')) {
                            demoddata_div.find('.ascii').hide();
                            demoddata_div.find('.ax25').hide();
                        }
                        else if ($('#ascii-button').hasClass('active')) {
                            demoddata_div.find('.hex').hide();
                            demoddata_div.find('.ax25').hide();
                        }
                        else {
                            demoddata_div.find('.hex').hide();
                            demoddata_div.find('.ascii').hide();
                        }

                    } else if (content_type == 'text') {
                        var utf_enc = new TextDecoder('utf-8');
                        demoddata_div.find('.data__body').append(`<span class="utf-8">${utf_enc.decode(this.response)}</span>`);
                    }
                    demoddata_div.show();
                    divs_shown += 1;
                    if (divs_shown == number_of_divs) {
                        var remaining_data = $('#data-tab .demoddata:hidden').length;
                        $('#next-data-num').text(Math.min(remaining_data, 10));
                        $('#all-data-num').text(remaining_data);
                        $('#demoddata-spinner').hide();
                        if (remaining_data > 0) {
                            $('#load-data-btn-group button').show();
                        }
                    }
                }
            };

            xhr.send();
        });
        $('#demoddata-spinner').hide();
    }

    load_demoddata(10, true);

    $('#load-next-10-button').click(function () {
        load_demoddata(10, false);
    });

    $('#load-all-button').click(function () {
        load_demoddata($('#data-tab .demoddata:hidden').length, false);
    });

    // Function to convert hex data in each data blob to ASCII, while storing
    // the original blob in a jquery .data, for later reversal back to hex
    // (see next function)
    $('#ascii-button').click(function () {
        $('.hex').hide();
        $('.ax25').hide();
        $('.ascii').show();
        $('#ascii-button').addClass('active');
        $('#hex-button').removeClass('active');
        $('#ax25-button').removeClass('active');
    });

    // retrieve saved hex data and replace the decoded blob with the original
    // hex text
    $('#hex-button').click(function () {
        $('.hex').show();
        $('.ascii').hide();
        $('.ax25').hide();
        $('#hex-button').addClass('active');
        $('#ascii-button').removeClass('active');
        $('#ax25-button').removeClass('active');
    });

    $('#ax25-button').click(function () {
        $('.hex').hide();
        $('.ascii').hide();
        $('.ax25').show();
        $('#ax25-button').addClass('active');
        $('#ascii-button').removeClass('active');
        $('#hex-button').removeClass('active');
    });

    // Hotkeys bindings
    $(document).unbind('keyup');
    $(document).bind('keyup', function (event) {
        if (event.which == 88) {
            var link_delete = $('#obs-delete');
            link_delete[0].click();
        } else if (event.which == 68) {
            var link_discuss = $('#obs-discuss');
            link_discuss[0].click();
        } else if (event.which == 85) {
            var link_unknown = $('#unknown-status');
            link_unknown[0].click();
        } else if (event.which == 71) {
            var link_good = $('#with-signal-status');
            link_good[0].click();
        } else if (event.which == 66) {
            var link_bad = $('#without-signal-status');
            link_bad[0].click();
        }
    });

    var navTabsBtn = document.querySelectorAll('[data-toggle="tab"]');
    for (let btn of navTabsBtn){
        btn.addEventListener('click', () => {
            if (!btn.parentElement.classList.contains('active')){
                for (let btn of navTabsBtn){
                    if(btn.parentElement.classList.contains('active')){
                        btn.parentElement.classList.remove('active');
                    }
                }
                btn.parentElement.classList.add('active');
            }
            var targetTab = btn.getAttribute('data-parent-tab');
            var target = btn.getAttribute('data-target-tab');
            var tabpane = document.querySelectorAll('#'+targetTab + ' .tab-pane');
            var tabpaneActive = document.querySelector('#'+targetTab + ' .tab-pane[id="'+target+'"]');
            for (let tab of tabpane){
                if (tab.classList.contains('active')){
                    tab.classList.remove('active');
                }
            }
            tabpaneActive.classList.add('active');
        });
    }
}

// copy to buffer function
function copyFunction(string) {
    const copyText = string;
    const textArea = document.createElement('textarea');
    textArea.textContent = copyText;
    document.body.append(textArea);
    textArea.select();
    document.execCommand('copy');
    textArea.remove();
}

Fancybox.bind('[data-fancybox]', {
    Toolbar: {
        display: {
            right: ['close'],
        },
    },
});